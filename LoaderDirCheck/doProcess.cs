﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using dnUtil;
using System.Data.OleDb;
using System.Data;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;
using System.Reflection;
using System.Globalization;

namespace LoaderDirCheck
{
    class doProcess
    {
        private string strMasterXLoaderPath;
        public string _strMasterXLoaderPath { get { return strMasterXLoaderPath; } set { strMasterXLoaderPath = value; } }
        private string strLogfilePath; 
        public string _strLogfilePath { get { return strLogfilePath; } set { strLogfilePath = value; } }
        private int intDaysToCheck; 
        public int _intDaysToCheck { get { return intDaysToCheck; } set { intDaysToCheck = value; } }
        private List<string> DirToCheck;
        private OleDbConnection cnMasterX;
        private string strProgramName = "DOCULOADERX";
        private System.Windows.Forms.ProgressBar PB;
        private System.Windows.Forms.DataGridView DGV;
        List<string> Errorlist;
        public event EventHandler stopPorcess = delegate { };
        private bool boolAport = false;
        private Boolean boolAutoStart; // 1.1.3 for testing the new autostart command line
        public Boolean _boolAutoStart { get { return boolAutoStart; } set { boolAutoStart = value; } }

        public doProcess()
        {
            strMasterXLoaderPath = findSettingVariable("LoaderXMasterPath");
            strLogfilePath = findSettingVariable("LogPath");
            intDaysToCheck = Convert.ToInt32((findSettingVariable("LengthInDaysToCheck")));
        }
        private static string findSettingVariable(string SettingName)
        {
            SettingName =  ConfigurationManager.AppSettings.Get(SettingName);
            return SettingName;
        }
        public void SaveConfig()
        {
            setConfig("LoaderXMasterPath", strMasterXLoaderPath);
            setConfig("LogPath", strLogfilePath);
            setConfig("LengthInDaysToCheck", intDaysToCheck.ToString());
        }

        private static void setConfig(string SettingName, string SettingValue)
        {
            if (SettingName != "")
                ConfigurationManager.AppSettings.Set(SettingName, SettingValue);
        }
        public void doDirSearch(System.Windows.Forms.ProgressBar Progress, System.Windows.Forms.DataGridView Folders)
        {
            boolAport = false;
            PB = Progress;
            DGV = Folders;
            findDirToCheck();
            docheckDir();

            if (PB.Value > 0)
                PB.Value = 0;

            if (!boolAport)
            {
                Errorlist.Sort();
                UtilFile.WriteSingleLineToFile("", doDateSub(strLogfilePath));
                //v 1.1.4 -- Environment.MachineName output each time now.
                UtilFile.WriteSingleLineToFile("Loader Directory Check (" +
                                               FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion
                                               + "): [" + Environment.MachineName + "] " + DateTime.Now.ToString(), doDateSub(strLogfilePath));
                UtilFile.WriteToFile(Errorlist, doDateSub(strLogfilePath));
                //dnUtil.UtilFile.WriteToFile(DirToCheck, "LoaderXPathsToCheck.txt");
            }
        }
        private void findDirToCheck()
        {
            cnMasterX = new OleDbConnection();
            if (dnUtil.UtilDB.DBOpen(out cnMasterX, strMasterXLoaderPath, false))
            {
                string strSQL = "";
                strSQL = strSQL + " SELECT OtherParameters FROM ProgramOptions WHERE UCASE(ProgramName) = '" + strProgramName + "';";
                if (dnUtil.UtilDB.TableIsThere(cnMasterX, "ProgramOptions"))
                {
                    DirToCheck = new List<string>();
                    DataTable DT = new DataTable();
                    DT = UtilDB.ExecuteQueryDataTable(cnMasterX, strSQL);
                    Application.DoEvents();
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        if (DT.Rows[i]["OtherParameters"].ToString() != "")
                            DirToCheck.Add(DT.Rows[i]["OtherParameters"].ToString());
                }}
                cnMasterX.Close();
        }}
        private void docheckDir()
        {
            stopPorcess += new EventHandler(doWorkflow_stopPorcess);
            
            DGV.Rows.Clear();
            DirToCheck.Sort();
            List<string> CheckedDir = new List<string>();
            Errorlist = new List<string>();
            PB.Maximum = DirToCheck.Count + 1;
            Application.DoEvents();

            foreach (string Dir in DirToCheck)
            {
                if (!boolAport)
                {
                    PB.Value = PB.Value + 1;
                    List<string> FolderPath = new List<string>(Dir.Split('|').ToList<string>());
                    Application.DoEvents();
                    foreach (string FolderDir in FolderPath)
                    {

                        string tempFolderDir = FolderDir.Trim();
                        if (tempFolderDir.Contains("\\"))
                        {
                            tempFolderDir = tempFolderDir.Substring(0,tempFolderDir.LastIndexOf('\\'));
                            List<string> tempSerachWildcards;
                            if (tempFolderDir.Contains('#'))
                            {
                                tempSerachWildcards = new List<string>(tempFolderDir.Substring(tempFolderDir.IndexOf('#')).Split('\\').ToList<string>());
                                tempFolderDir = tempFolderDir.Substring(0, tempFolderDir.IndexOf('#'));
                            }
                            else
                                tempSerachWildcards = new List<string>();
                            //tempSerachWildcards = tempSerachWildcards.Replace('#', '?');
                        
                            tempFolderDir = UtilFile.DirWithSlash(tempFolderDir);
                            if (Directory.Exists(tempFolderDir))
                            {
                                if (!CheckedDir.Contains(tempFolderDir))
                                {
                                    CheckedDir.Add(tempFolderDir);
                                    if (tempSerachWildcards.Count == 0)
                                    {
                                        List<String> BatchList = new List<string>(Directory.GetDirectories(tempFolderDir));//, "\\" + tempSerachWildcards + "\\", SearchOption.AllDirectories));
                                        Application.DoEvents();
                                        foreach (string tempBatch in BatchList)
                                        {
                                            if (Directory.GetCreationTime(tempBatch) >= DateTime.Now.AddDays(-intDaysToCheck))
                                            {
                                                List<String> FileList = new List<string>(Directory.GetFiles(tempBatch));//, "\\" + tempSerachWildcards + "\\", SearchOption.AllDirectories));
                                                if (FileList.Count != 0)
                                                {
                                                    //if (DirList.Count != 0)
                                                    //{
                                                    //if (!DirList.Contains("FixImages"))
                                                    //{
                                                    if (tempFolderDir.EndsWith("\\PDF\\"))
                                                    {
                                                        if (!tempBatch.ToUpper().EndsWith("\\DONE"))
                                                        {
                                                            if (!PDF_Check(FileList))
                                                            {
                                                                Errorlist.Add(tempBatch);
                                                                DGV.Rows.Add(tempBatch);
                                                                Application.DoEvents();
                                                            }
                                                        }
                                                    }
                                                    else if (!FileList.Contains(tempBatch + "\\BatchLoader.idx")) //if (!FileList.Contains(".idx"))
                                                    {
                                                        Errorlist.Add(tempBatch);
                                                        DGV.Rows.Add(tempBatch);
                                                        Application.DoEvents();
                                    }}}}}//}}
                                    else if (tempSerachWildcards.Count == 1)
                                    {
                                        List<string> DateDir = new List<string>(Directory.GetDirectories(tempFolderDir));
                                        Application.DoEvents();
                                        foreach (string tempDate in DateDir)
                                        {
                                            if (Directory.Exists(tempDate))
                                            {

                                                List<String> BatchList = new List<string>(Directory.GetDirectories(tempDate));
                                                Application.DoEvents();
                                                foreach (string tempBatch in BatchList)
                                                {
                                                    DateTime temp1 = Directory.GetCreationTime(tempBatch);
                                                    DateTime temp2 = DateTime.Now.AddDays(-intDaysToCheck);
                                                    if (temp1 >= temp2)
                                                    {
                                                        List<String> FileList = new List<string>(Directory.GetFiles(tempBatch));
                                                        if (FileList.Count != 0)
                                                        {

                                                            //if (DirList.Count != 0)
                                                            //{
                                                            //if (!DirList.Contains("FixImages"))
                                                            //{
                                                            if (!FileList.Contains(tempBatch + "\\BatchLoader.idx"))
                                                            {
                                                                Errorlist.Add(tempBatch);
                                                                DGV.Rows.Add(tempBatch);
                                                                Application.DoEvents();
                                                            
                                    }}}}}}}//}
                                    else if (tempSerachWildcards.Count == 2)
                                    {
                                        List<string> DateDir = new List<string>(Directory.GetDirectories(tempFolderDir));
                                        Application.DoEvents();
                                        foreach (string tempDate in DateDir)
                                        {
                                            if (Directory.Exists(tempDate))
                                            {
                                                List<string> ExtraDir = new List<string>(Directory.GetDirectories(tempDate));
                                                Application.DoEvents();
                                                foreach (string tempExtraDir in ExtraDir)
                                                {
                                                    if (Directory.Exists(tempExtraDir))
                                                    {
                                                        List<String> BatchList = new List<string>(Directory.GetDirectories(tempExtraDir));
                                                        Application.DoEvents();
                                                        foreach (string tempBatch in BatchList)
                                                        {
                                                            if (Directory.GetCreationTime(tempBatch) >= DateTime.Now.AddDays(-intDaysToCheck))
                                                            {
                                                                List<String> FileList = new List<string>(Directory.GetFiles(tempBatch));
                                                                if (FileList.Count != 0)
                                                                {

                                                                    //if (DirList.Count != 0)
                                                                    //{
                                                                    // if (!DirList.Contains("FixImages"))
                                                                    //{
                                                                    if (!FileList.Contains(tempBatch + "\\BatchLoader.idx")) //if (!FileList.Contains(".idx"))
                                                                    {
                                                                        Errorlist.Add(tempBatch);
                                                                        DGV.Rows.Add(tempBatch);
                                                                        Application.DoEvents();
                                        }}}}}}}}}
                                        else if (tempSerachWildcards.Count > 2)
                                        {
                                            DoCurrentSubfolderCheck(tempFolderDir);
        }}}}}}}}//}}

        private bool PDF_Check(List<string> fileList)
        {
            // 1.1.6 return false if there is at least one PDF file in fileList
            // intended for SFHP to find any unprocessed PDF files
            foreach (string file in fileList)
            {
                if (file.ToUpper().EndsWith(".PDF"))
                    return false;
            }
            return true;
        }

        private void DoCurrentSubfolderCheck(string currentSubFolder)
        {
            if (currentSubFolder.ToUpper().EndsWith("BACKUP") || currentSubFolder.ToUpper().EndsWith("BACKUP\\"))
                return;

            if (!Directory.Exists(currentSubFolder))
                return;

            bool foundTiff = false, foundIDX = false;

            if (!currentSubFolder.ToUpper().Contains("FIXIMAGES"))
            {
                if (Directory.GetCreationTime(currentSubFolder) >= DateTime.Now.AddDays(-intDaysToCheck))
                {
                    List<string> files = new List<string>(Directory.GetFiles(currentSubFolder));
                    foreach (string file in files)
                    {
                        if (file.ToUpper().EndsWith(".TIF"))
                            foundTiff = true;

                        if (file.ToUpper().EndsWith(".IDX"))
                            foundIDX = true;

                        if (foundTiff && foundIDX)
                            break;
                    }

                    if(foundTiff && !foundIDX)
                    {
                        // the current subfolder contain .tif images but not a .idx file, report the current subfolder.
                        Errorlist.Add(currentSubFolder);
                        DGV.Rows.Add(currentSubFolder);
                        Application.DoEvents();
                    }
                }
            }

            List<string> containedSubfolders = new List<string>(Directory.GetDirectories(currentSubFolder));

            foreach (string containedSubFolder in containedSubfolders)
                DoCurrentSubfolderCheck(containedSubFolder);

        }

        public void openErrorLog(string filePath)
        {
            if (Directory.Exists(filePath))
                System.Diagnostics.Process.Start(filePath);
            if (File.Exists(doDateSub(filePath)))
                System.Diagnostics.Process.Start(doDateSub(filePath));
        }
        private string doDateSub(string FileName)
        {
            if (FileName.Contains("[MMDDYYYY]"))
            {
                //string temp = DateTime.Now.ToShortDateString();
                string temp = DateTime.Now.ToString("MMddyyyy");
                //temp = temp.Replace("/", "");
                FileName = FileName.Replace("[MMDDYYYY]",temp);
            }
            return FileName;
        }
        public void StopTheProgram()
        {
            stopPorcess(this, null);
        }
        void doWorkflow_stopPorcess(object sender, EventArgs e)
        {
            boolAport = true;
        }
        public string findSelectedImage(DataGridView DGV)
        {
            return (UtilDGV.formFindSelectedImageName(DGV));
        }
    }
}
