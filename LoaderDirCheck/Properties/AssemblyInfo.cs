﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("LoaderDirCheck")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Windows User")]
[assembly: AssemblyProduct("LoaderDirCheck")]
[assembly: AssemblyCopyright("Copyright © Windows User 2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("251e0282-785b-4200-85fb-da630266e897")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.1.6")]
[assembly: AssemblyFileVersion("1.1.6")]
// v1.1.3 01/25/2018 - Added the ability to read a command line switches
// v1.1.4 08/07/2018 - Environment.MachineName output in report log each time now.
// v1.1.5 10/25/2020 - Added support for the case where the count/depth of tempSerachWildcards is 3 or more layers
// v1.1.6 11/10/2020 - Added support for PDF scan project to detect unprocessed PDF files, intended for SFHP, currently to just 1 layer 