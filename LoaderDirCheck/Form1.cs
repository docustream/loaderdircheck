﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LoaderDirCheck
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private doProcess doProgram;
        private void Form1_Load(object sender, EventArgs e)
        {
            doProgram = new doProcess();
            txtLoaderXPath.Text = doProgram._strMasterXLoaderPath;
            txtLogFilePath.Text = doProgram._strLogfilePath;
            string strErr = parseCommandLine();  // 1.1.3
            if (strErr != "")
            {
                MessageBox.Show(strErr, "Command Line Error");
                btnStart.Enabled = false;
                btnViewLog.Enabled = false;
                lblStatus.Text = "ERROR: Command line switches not set up properly";
            }
            else
            {
                if (doProgram._boolAutoStart)  // 1.1.3
                {
                    timer1.Interval = 500;  // set 500ms seconds for the Form to completely load and appear on the screen?
                    timer1.Enabled = true;
                };
            }
        }

        private void saveSettings()
        {
            doProgram._strMasterXLoaderPath = txtLoaderXPath.Text;
            doProgram._strLogfilePath = txtLogFilePath.Text;
            doProgram.SaveConfig();
        }
        private void btnStart_Click(object sender, EventArgs e)
        {
            btnStart.Enabled = false;
            btnViewLog.Enabled = false;
            String strErr = "";
            if (!dnUtil.UtilFile.FileIsThere(txtLoaderXPath.Text))
            {
                strErr = strErr + "\n" + "ERROR: LoaderXMaster database is not found. Check LoaderDBPath setting.";
            }
            if (txtLogFilePath.Text == "")
            {
                //If the log file path is not specified, set the default log output to the same folder as the program EXE
                txtLogFilePath.Text = "[MMDDYYYY]LoaderDirCheckLog.txt";
            }
            if (strErr != "")
            { 
                // don't run the program if the program is not set up properly
                MessageBox.Show(strErr, "ERROR: Program Not Set Up Properly");
                lblStatus.Text = "ERROR: Program is not set up properly.";
            }
            else
            {
                saveSettings();
                lblStatus.Text = "Progress: Checking directories";
                doProgram.doDirSearch(PB, DGVFolders);
                lblStatus.Text = "Progress: Finnished";
                btnStart.Enabled = true;
                btnViewLog.Enabled = true;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            saveSettings();
            this.Close();
            Application.Exit();
        }

        private void btnViewLog_Click(object sender, EventArgs e)
        {
            doProgram.openErrorLog(doProgram._strLogfilePath);
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            doProgram.StopTheProgram();
            lblStatus.Text = "Progress: Stopped";
            PB.Value = 0;
            btnStart.Enabled = true;
            btnViewLog.Enabled = true;
            timer1.Enabled = false;
        }

        private void MenuItemCopy_Click(object sender, EventArgs e)
        {
            if (DGVFolders.SelectedRows.Count > 0)
                Clipboard.SetText(DGVFolders.SelectedRows[0].Cells["clmFolder"].Value.ToString());
            if (DGVFolders.SelectedCells.Count > 0)
                Clipboard.SetText(DGVFolders.SelectedCells[0].Value.ToString());
            // doProgram.findSelectedImage(DGVFolders));
        }

        private void MenuItemOpen_Click(object sender, EventArgs e)
        {
            if (DGVFolders.SelectedRows.Count > 0)
                doProgram.openErrorLog(DGVFolders.SelectedRows[0].Cells["clmFolder"].Value.ToString());
            if (DGVFolders.SelectedCells.Count > 0)
                doProgram.openErrorLog(DGVFolders.SelectedCells[0].Value.ToString());
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            btnStart_Click(sender,System.EventArgs.Empty);  // 1.1.3
        }

        private string parseCommandLine()
        {
            // 1.1.3 reads the command line switches
            string strError = "";

            string[] arrArgs = Environment.GetCommandLineArgs();
            
            for (int i = 0; i < arrArgs.Length; i++)
            {
                string strArg = arrArgs[i].ToUpper();
                switch (strArg)
                {
                    case "-AUTOSTART":
                        doProgram._boolAutoStart = true;
                        break;
                    case "-LOADERDBPATH":
                        if (i == arrArgs.Length - 1 || arrArgs[i + 1].StartsWith("-") || arrArgs[i + 1].Trim() == "")
                        {
                            strError = "COMMAND LINE ERROR: LoaderDB Path not specified";
                        }
                        else
                        {
                            txtLoaderXPath.Text = arrArgs[++i];
                        }
                        break;
                    case "-LOG":
                        if (i == arrArgs.Length - 1 || arrArgs[i + 1].StartsWith("-") || arrArgs[i + 1].Trim() == "")
                        {
                            strError = "COMMAND LINE ERROR: Log File Path not specified";
                        }
                        else
                        {
                            txtLogFilePath.Text = arrArgs[++i];
                        }
                        break;
                }
            }
            return strError;
        }
    }
}
