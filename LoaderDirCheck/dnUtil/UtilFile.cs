using System;
using System.Collections;
using System.IO;
using System.Windows.Forms;

using System.Diagnostics;
namespace dnUtil
{
    /// <summary>
    /// 
    /// </summary>

    public class UtilFile
    {
        public UtilFile()
        {
        }
        #region Directory: \\ Functions
        /// <summary>
        /// Return a Directory with a \\ added at the end
        /// </summary>
        /// <param name="strDir">Directory Path</param>
        /// <returns>Dir with \\ added to end</returns>
        public static String DirWithSlash(String strDir)
        {
            String strTemp;
            if (strDir == "")
                return "";
            strTemp = strDir.Substring(strDir.Length - 1, 1);
            if (strTemp != "\\")
                return strDir + "\\";
            else
                return strDir;
        }
        /// <summary>
        /// Return a Directory without a \\ at the end
        /// </summary>
        /// <param name="strDir">Directory Path</param>
        /// <returns>Dir with \\ Removed from end</returns>
        public static String DirNoSlash(String strDir)
        {
            if ((strDir.Substring(strDir.Length - 1, 1) != "\\"))
                return strDir;
            if (strDir.Length == 1)
                return "";
            return strDir.Substring(0, strDir.Length - 1);
        }
        #endregion
        #region Directory: Change DirName
        /// <summary>
        /// Take a path and replace the last folder with a new folder
        /// EI DocuHealthlink\Archive to DocuHealthlink\Thumbnails
        /// </summary>
        /// <param name="Path">Path (ei C:\\Test\\Program\\001\\)</param>
        /// <param name="Orig">Original string to replace (ei "Test") [No "\\" included]</param>
        /// <param name="Destination">New string to insert (ei "New") [No "\\" included]</param>
        /// <returns>Path with new Destination inserted for Original (ei C:\\New\\Program\\001\\)</returns>
        public static string ChangeDir(string Path, string Orig, string Destination)
        {
            string newPath;

            newPath = Path.Replace("\\" + Orig + "\\", "\\" + Destination + "\\");
            return newPath;
        }
        #endregion
        #region File: Read from File Functions
        /// <summary>
        /// Read from a file, similar to an INI read
        /// </summary>
        /// <param name="strSegment">Segment to look for (splits with a "=")</param>
        /// <param name="strFile">File path</param>
        /// <returns>File text on line after "Segment="</returns>
        public static string ReadFromFile(String strSegment, String strFile)
        {
            try
            {
                System.IO.StreamReader Mystream;
                if (System.IO.File.Exists(strFile) == false)
                {
                    return "N/A";
                }
                else
                {

                }
                Mystream = new StreamReader(strFile);
                String strText;
                while (Mystream.EndOfStream == false)
                {
                    string[] arrSplit;
                    strText = Mystream.ReadLine();
                    arrSplit = strText.Split('=');
                    if (arrSplit.Length > 1)
                    {
                        if (arrSplit[0] == strSegment)
                        {
                            Mystream.Close();
                            Mystream.Dispose();
                            Mystream = null;
                            return arrSplit[1];
                        }
                    }
                }
                Mystream.Close();
                Mystream.Dispose();
                Mystream = null;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return "";
        }
        /// <summary>
        /// Return the whole file in a string array
        /// </summary>
        /// <param name="strFile">File Path</param>
        /// <returns>string[] with each line of the file</returns>
        public static string[] ReadFromFile(string strFile)
        {
            try
            {
                if (System.IO.File.Exists(strFile) == false)
                { }
                else
                { }
                return File.ReadAllLines(strFile);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return null;
        }
        /// <summary>
        /// Read from a file, similar to an INI read
        /// </summary>
        /// <param name="strSegment">Setting Searched for (not including "=")</param>
        /// <param name="strFile">File Name</param>
        /// <param name="strSection">[Section] to search for setting</param>
        /// <returns></returns>
        public static string ReadFromFile(string strSegment, string strFile, string strSection, bool SkipMissing = false)
        {
            //strSegment = strSegment.ToUpper();
            //strSection = strSection.ToUpper();
            string currentSection = "";
            try
            {
                System.IO.StreamReader Mystream;
                if (System.IO.File.Exists(strFile) == false)
                {
                    return "N/A";
                }
                else
                {

                }
                Mystream = new StreamReader(strFile);
                string strText;
                string strKeep;
                while (Mystream.EndOfStream == false)
                {
                    strText = Mystream.ReadLine();
                    strKeep = strText;
                    strText = strText.ToUpper();
                    if (strText.Contains("["))
                    {
                        currentSection = strText;
                    }
                    while (currentSection == "[" + strSection.ToUpper() + "]")
                    {
                        strText = Mystream.ReadLine();
                        strKeep = strText;
                        strText = strText.ToUpper();
                        if (strText.Contains("["))
                        {
                            currentSection = strText;
                        }
                        string[] arrSplit;
                        arrSplit = strKeep.Split('=');
                        if (arrSplit.Length > 1)
                        {
                            if (arrSplit[0].ToUpper() == strSegment.ToUpper())
                            {
                                Mystream.Close();
                                Mystream.Dispose();
                                Mystream = null;
                                return arrSplit[1];
                            }
                        }

                    }
                }
                Mystream.Close();
                Mystream.Dispose();
                Mystream = null;
            }
            catch (Exception)
            {
                if (!SkipMissing)
                    MessageBox.Show(strSegment + " Does not exist in File");
                //MessageBox.Show(e.Message);
            }
            return "";
        }
        #endregion
        #region File: Write to file Fucntions
        /// <summary>
        /// Write single line to file
        /// </summary>
        /// <param name="strText">Text to write to file</param>
        /// <param name="strFile">File path</param>
        public static void WriteToFile(String strText, String strFile)
        {
            try
            {
                System.IO.StreamWriter Mystream;
                if (System.IO.File.Exists(strFile) == false)
                {
                    Mystream = File.CreateText(strFile);
                }
                else
                {
                    Mystream = new StreamWriter(strFile, true);
                }
                Mystream.WriteLine(strText);

                Mystream.Close();
                Mystream.Dispose();
                Mystream = null;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        /// <summary>
        /// Write an error to the log
        /// </summary>
        /// <param name="strText">Text to write to LogFile</param>
        /// <param name="logFile">File Path</param>
        public static void WriteLogDetail(string strText, string logFile)
        {
            WriteSingleLineToFile(DateTime.Now.ToString() + "   " + strText, logFile);
        }
        /// <summary>
        /// Write a single line to the Text File
        /// </summary>
        /// <param name="strText">Text to Write</param>
        /// <param name="logFile">File Path</param>
        public static void WriteSingleLineToFile(string strText, string logFile)
        {
            string[] arr = { strText };
            WriteToFile(strText, logFile);
        }
        /// <summary>
        /// Write an array to a string file
        /// </summary>
        /// <param name="strText">String[] to write to file</param>
        /// <param name="strFile">File Path</param>
        public static void WriteToFile(String[] strText, String strFile)
        {
            try
            {
                System.IO.StreamWriter Mystream;
                if (System.IO.File.Exists(strFile) == false)
                {
                    Mystream = File.CreateText(strFile);
                }
                else
                {
                    Mystream = new StreamWriter(strFile, true);
                }
                foreach (string str in strText)
                {
                    Mystream.WriteLine(str);
                }
                Mystream.Close();
                Mystream.Dispose();
                Mystream = null;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        /// <summary>
        /// Write an array to a string file
        /// </summary>
        /// <param name="strText">String[] to write to file</param>
        /// <param name="strFile">File Path</param>
        public static void WriteToFile(System.Collections.Generic.List<string> strText, String strFile)
        {
            try
            {
                System.IO.StreamWriter Mystream;
                if (System.IO.File.Exists(strFile) == false)
                {
                    Mystream = File.CreateText(strFile);
                }
                else
                {
                    Mystream = new StreamWriter(strFile, true);
                }
                foreach (string str in strText)
                {
                    Mystream.WriteLine(str);
                }
                Mystream.Close();
                Mystream.Dispose();
                Mystream = null;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        #endregion
        #region File: DeleteFile
        /// <summary>
        /// Delete the File w/ Try Catch
        /// </summary>
        /// <param name="strFile">File Path</param>
        public static bool KillFile(String strFile)
        {
            try
            {
                File.Delete(strFile);
                return true;
            }
            catch
            { return false; }
        }
        #endregion
        #region File: File is Locked
        /// <summary>
        /// Check to see if the file is locked and attempt to close it
        /// </summary>
        /// <param name="strFullName">File Path</param>
        /// <returns>True: File is locked, false: file not locked</returns>
        public static bool FileIsLocked(string strFullName)
        {
            Boolean blnReturn = false;
            System.IO.FileStream fs;
            try
            {
                fs = System.IO.File.Open(strFullName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.None);
                fs.Close();
            }
            catch
            {
                blnReturn = true;
            }
            return blnReturn;
        }
        #endregion
        #region File: File Length
        /// <summary>
        /// Return the file length
        /// </summary>
        /// <param name="strFile">File Path</param>
        /// <returns>Long length of File</returns>
        public static long FileLen(String strFile)
        {
            if (!File.Exists(strFile))
                return -1;

            FileInfo fi = new FileInfo(strFile);
            return fi.Length;
        }
        #endregion
        #region File: File Replace
        /// <summary>
        /// Replace one file with a second
        /// </summary>
        /// <param name="SourceFile1">Source File</param>
        /// <param name="DesFile2">Destenation File</param>
        /// <param name="BkFile">Backup file name (Defaults to null)</param>
        public static void swapFiles(string SourceFile1, string DesFile2, string BkFile = null)
        {
            File.Replace(SourceFile1, DesFile2, BkFile);
        }
        #endregion
        #region File: RenameClaims
        /// <summary>
        /// Rename Claim, txt and Thumbnail
        /// </summary>
        /// <param name="OrigFile">Location of current Claim</param>
        /// <param name="DestFile">Destenation of Claim</param>
        public static void doRenameClaims(string OrigFile, string DestFile)
        {
            try
            {
                if (File.Exists(OrigFile))
                    File.Move(OrigFile, DestFile);
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }
        #endregion
        #region File: DeleteClaims
        /// <summary>
        /// Delete Claim from path
        /// </summary>
        /// <param name="Claim">Path of claim to delete</param>
        public static void DeleteClaims(string Claim)
        {
            try
            {

                if (File.Exists(Claim))
                    File.Delete(Claim);
            }
            catch
            { }
        }
        #endregion
        #region Path: Find in Tif Name
        /// <summary>
        /// Find Name minus the enummorator at the end
        /// </summary>
        /// <param name="tifLocation">Full Tif Path</param>
        /// <returns>File name minus Number</returns>
        public static string findNameMinusNum(string tifLocation)
        {
            string TifName;
            TifName = tifLocation.Remove(tifLocation.LastIndexOf('_') + 1);
            return TifName;
        }
        /// <summary>
        /// Find the enummorator at the end of the tif name
        /// </summary>
        /// <param name="tifLocation">Full Tif Path</param>
        /// <returns>Nuber at end of file</returns>
        public static int findNameNum(string tifLocation)
        {
            string TifNum;
            TifNum = tifLocation.Substring(tifLocation.LastIndexOf('_') + 1, 4);
            return Convert.ToInt32(TifNum);
        }
        /// <summary>
        /// Find Payer ID from tif name (last 4 before '_')
        /// </summary>
        /// <param name="tifLocation">Location of Tif Image</param>
        /// <returns>4 digit PayerID</returns>
        public static string findNamePayer(string tifLocation)
        {
            string TifPayer;
            TifPayer = tifLocation.Substring(tifLocation.LastIndexOf('_') - 4, 4);
            if (TifPayer.ToUpper() == "XXX")
                TifPayer = "";
            return TifPayer;
        }
        /// <summary>
        /// Return the name plus the added number
        /// </summary>
        /// <param name="tifLocation">Location of Tif Image</param>
        /// <param name="AddNum">Number to add on End</param>
        /// <returns>new Tif number with number added on end</returns>
        public static string newNamePlusNum(string tifLocation, int AddNum = 1)
        {
            string Extension = Path.GetExtension(tifLocation);
            string tempName = findNameMinusNum(tifLocation);
            int tempNum = findNameNum(tifLocation);
            string Num = (tempNum + AddNum).ToString("D4");
            return tempName + Num + Extension;
        }
        #endregion
        #region Path: FileNames
        /// <summary>
        /// Remove the Directory and extention from a path and return the file name
        /// </summary>
        /// <param name="strFile">File Path</param>
        /// <returns>Name minus Ext and Dir</returns>
        public static String FileName(String strFile)
        {
            string[] arr;
            string retVal;
            retVal = "";
            int intPos;
            intPos = strFile.IndexOf(".");
            if (intPos <= 1)
            {
                return retVal;
            }
            retVal = strFile.Substring(1, intPos - 1);
            arr = strFile.Split('\\');
            retVal = arr[arr.Length - 1].ToString();
            return retVal;
        }
        /// <summary>
        /// Return the folder and the file name EX: YYYYMMDD/FileName.txt
        /// </summary>
        /// <param name="strFile">File Path</param>
        /// <returns>SubFolder, File and extension</returns>
        public static String FolderFileName(String strFile)
        {
            string[] arr;
            string retVal;
            retVal = "";
            int intPos;
            intPos = strFile.IndexOf(".");
            if (intPos <= 1)
            {
                return retVal;
            }
            retVal = strFile.Substring(1, intPos - 1);
            arr = strFile.Split('\\');
            retVal = arr[arr.Length - 2].ToString() + "\\";
            retVal = retVal + arr[arr.Length - 1].ToString();
            return retVal;
        }
        /// <summary>
        /// Return the name Path subsitiuting .Txt to the extension
        /// </summary>
        /// <param name="str">FilePath</param>
        /// <returns>FilePath with a .Txt extension</returns>
        public static string ReturnTxtName(string str, string ext = ".txt")
        {
            string pathDir = UtilFile.DirWithSlash(Path.GetDirectoryName(str));
            string Txtfile = Path.GetFileNameWithoutExtension(str) + ext;
            return pathDir + Txtfile;
        }
        #endregion

        public static bool FileIsThere(String strFile)
        {
            return File.Exists(strFile);
        }

        public static bool DirIsThere(String strdirectory)
        {
            return Directory.Exists(strdirectory);
        }

        public static bool MakePath(String strdirectory)
        {
            if (!DirIsThere(strdirectory))
            {
                Directory.CreateDirectory(strdirectory);
                return DirIsThere(strdirectory) ? true : false;
            }
            return true;
        }
        public static bool IsDirectory(String strFile)
        {
            if (UtilFile.FileIsThere(strFile) || UtilFile.DirIsThere(strFile))
            {
                return ((File.GetAttributes(strFile) & FileAttributes.Directory) == FileAttributes.Directory);
            }
            else
            {
                return false;
            }
        }
    }
}
