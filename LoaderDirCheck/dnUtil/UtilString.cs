using System;
using System.Text.RegularExpressions;
using System.Collections;
using Microsoft.Win32;
using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace dnUtil
{
	/// <summary>
	/// Summary description for UtilString.
	/// </summary>
	public class UtilString
	{
		// for encryption (only 8 characters used)
		private const string ENCRYPT_KEY = "d0cu5tr3am";

		public UtilString()
		{
		}

		public static String CharListPunct()
		{ return CharListPunct(true); }
		public static String CharListPunct(bool boolIncludeNeg)
		{
			return ".,;'\"!_-@#$%^&*()+=\\/|?><`~[]{}: \r\n�";
		}

		public static String CharListInt(bool IncludeDec, bool IncludeNeg)
		{
			string strRet= "01232456789";
			if(IncludeDec)
			{
				strRet += ".";
			}
			if(IncludeNeg)
			{
				strRet += "-";
			}
			
			return strRet;
		}

		public static String CharListAlpha()
		{
			return "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		}

		/// <summary>
		/// Replaces all bad characters with spaces
		/// </summary>
		public static string CleanString(string str)
		{
			StringBuilder sb = new StringBuilder(str.Length);
			for(int i=0; i < str.Length; i++)
			{
				int intAscii = UtilString.Ascii(str[i]);
				
				if(intAscii >= 32 && intAscii <= 126) // valid ascii range: 32-126
					sb.Append(str[i]);
				else if(intAscii == 10 || intAscii == 13) // carriage return and line feed are ok ("\n" = 10, "\r" = 13)
					sb.Append(str[i]);
				else
					sb.Append(' ');
			}

			return sb.ToString();
		}

		public static int Count(String strSource, String strCount)
		{
			int i;
			int c;
			int lngCount;
			c = 1;
			int retVal;
			if(strCount == "")
			{
				return 0;
			}
    
			lngCount = 0;
			i = UtilString.InStr(c, strSource, strCount);
    
			while(i != 0)
			{
				lngCount = lngCount + 1;
				c = i + strCount.Length;
				i = UtilString.InStr(c, strSource, strCount);
			}	
    
			retVal = lngCount;
			return retVal;
		}

        /// <summary>
        /// return a strNum / 100
        /// </summary>
        public static string DecNumber(string strNum)
        {
            int intTemp = ToNumberDefault(strNum, 0);
            if (intTemp == 0)
            {
                return "0.00";
            }

            Decimal dcTemp = (decimal)intTemp / 100;
            return dcTemp.ToString("#,##0.00");
        }

		public static String DelimField(String str, String strDelim, int intField)
		{
			String strTemp = "";
			int i;
			int intTemp1;

			int intLen = str.Length;
			int intLenDelim = strDelim.Length;

			if(intLen==0 || intLenDelim==0 || intField <=0)
			{
				return "";
			}

			int intCount = Count(str, strDelim);
			if(intCount==0 && intField==1)
			{
				return str;
			}

			if(intField > intCount + 1)
			{
				return "";
			}

			int intCurrPos=1;

			i = 1;
			while(intCurrPos <= intField)
			{
				intTemp1 = InStr(i, str, strDelim);
				if(intTemp1<=0)	
				{
					if(intCurrPos==intField)
					{
						strTemp= Mid(str, i, intLen - i + 1);
					}
					return strTemp;
				}
				else
				{

					strTemp= Mid(str, i, intTemp1 - i);

					if(intCurrPos==intField)
					{	
						return strTemp;
					}
					i = intTemp1 + intLenDelim;
					intCurrPos = intCurrPos + 1;
				}
			}

			return "";
		}

		public static String DelimFieldFromEnd(String str, char strDelim, int intField)
		{
			string []arrTemp = str.Split(strDelim);
			String strRet;
			if(arrTemp.Length > 0)
			{
				if(intField >= 1 && intField <= arrTemp.Length)
				{
					strRet = arrTemp[arrTemp.Length - intField];
				}
				else
				{
					strRet = "";
				}
				return strRet;
			}
			else
			{
				return "";
			}
		}		
		
		public static String FillChars(String strToAppendTo, String strChar, int intTotalLength, bool boolFront)
		{
			while( strToAppendTo.Length < intTotalLength)
			{
				if(boolFront)
				{
					strToAppendTo = strChar + strToAppendTo;
				}
				else
				{
					//'append to back
					strToAppendTo = strToAppendTo + strChar;
				}
			}

			return strToAppendTo;
		}

		public static bool IsInteger(String strCheck)
		{
			double dbl;
			return Double.TryParse(strCheck, System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.CurrentInfo, out dbl);
		}

		public static bool IsDouble(String strCheck)
		{
			double dbl;
			return Double.TryParse(strCheck, System.Globalization.NumberStyles.Float, System.Globalization.NumberFormatInfo.CurrentInfo, out dbl);
		}

		public static bool IsCurrency(String strCheck)
		{
			double dbl;
			return Double.TryParse(strCheck, System.Globalization.NumberStyles.Currency, System.Globalization.CultureInfo.CurrentCulture, out dbl);
		}

		public static bool IsNumber(String strCheck)
		{
			int intTemp;
			try
			{
				intTemp = Convert.ToInt32(strCheck);
				return true;
			}
			catch
			{
				return false;
			}
		}
		
		/// <summary>
		/// Runs 'IsNumber' on each character.  strNum cannot have decimals or negatives
		/// </summary>
		public static bool IsNumberLong(String strNum)
		{
			int i;
			if(strNum == "")
			{
				return true;
			}
			for(i = 0; i<strNum.Length; i++)
			{
				if(!UtilString.IsNumber(strNum.Substring(i, 1)))
				{
					return false;
				}
			}
			return true;
		}

		public static bool IsCash(char ch, bool boolAllowNegative)
		{
			if(CharListInt(true, boolAllowNegative).IndexOf(ch) >= 0)
				return true;
			return false;
		}

		public static bool IsPunct(char ch)
		{ return IsPunct(ch, false); }
		public static bool IsPunct(char ch, bool boolAllowNegative)
		{
			if(CharListPunct(!boolAllowNegative).IndexOf(ch) >= 0)
				return true;
			return false;
		}

		public static bool HasPunct(string str)
		{ return HasPunct(str, false); }
		public static bool HasPunct(string str, bool boolAllowNegative)
		{
			for(int i=0; i < str.Length; i++)
				if(UtilString.IsPunct(str[i], boolAllowNegative))
					return true;

			return false;
		}

		public static bool HasAnyOf(string strChars, string str)
		{
			for(int i=0; i < str.Length; i++)
				if(strChars.IndexOf(str[i]) != 0)
					return true;

			return false;
		}

		/// <summary>
		/// returns true if str contains a character that is not in the valid ascii range: 32-126
		/// </summary>
		public static bool HasBadChar(string str)
		{
			int intAscii;

			for(int i=0; i < str.Length; i++)
			{
				intAscii = UtilString.Ascii(str[i]);

				// carriage return and line feed are ok ("\n" = 10, "\r" = 13)
				if(intAscii == 10 || intAscii == 13)
					continue;

				// valid ascii range: 32-126
				if(intAscii < 32 || intAscii > 126)
					return true;
			}

			return false;
		}
		
		public static int InStrRev(String strValue, String strFind)
		{
			return strValue.LastIndexOf(strFind) + 1;
		}
		
		public static int InStr(int intStart, String strValue, String strFind)
		{
			return strValue.IndexOf(strFind, intStart - 1) + 1;
		}

		/// <summary>
		/// Returns true if there is a matching string found that is not within parentheses
		/// </summary>
		public static bool IsOutsideParentheses(string str, string strToFind)
		{
			if(str.Length < 1)
				return false;

			// first remove everything within parentheses, then find the string
			Stack stack = new Stack();
			StringBuilder sb = new StringBuilder(str.Length);

			for(int i=0; i < str.Length; i++)
			{
				if(str[i] == '(')
					stack.Push('(');
				else if(str[i] == ')')
					stack.Pop();
				else if(stack.Count == 0)
					sb.Append(str[i]);
			}

			return (sb.ToString().IndexOf(strToFind) >= 0) ? true : false;
		}

		/// <summary>
		/// Returns true if there is a matching string found that is not within quotes
		/// </summary>
		public static bool IsOutsideQuotes(string str, string strToFind)
		{
			if(str.Length < 1)
				return false;

			// first remove everything within quotes, then find the string
			Stack stack = new Stack();
			StringBuilder sb = new StringBuilder(str.Length);

			for(int i=0; i < str.Length; i++)
			{
				if(str[i] == '"')
				{
					if(stack.Count == 0)
						stack.Push(str[i]);
					else if(stack.Peek().ToString() == "\"")
						stack.Pop();
				}
				else if(str[i] == '\'')
				{
					if(stack.Count == 0)
						stack.Push(str[i]);
					else if(stack.Peek().ToString() == "'")
						stack.Pop();
				}
				else if(stack.Count == 0)
					sb.Append(str[i]);
			}

			return (sb.ToString().IndexOf(strToFind) >= 0) ? true : false;
		}

		/// <summary>
		/// Returns true if the index given is within quotes
		/// </summary>
		public static bool IsOutsideQuotes(string str, int intIndex)
		{
			if(str.Length < 1 || intIndex >= str.Length)
				return true;

			Stack stack = new Stack();

			for(int i=0; i < str.Length; i++)
			{
				if(str[i] == '"')
				{
					if(stack.Count == 0)
						stack.Push(str[i]);
					else if(stack.Peek().ToString() == "\"")
						stack.Pop();
				}
				else if(str[i] == '\'')
				{
					if(stack.Count == 0)
						stack.Push(str[i]);
					else if(stack.Peek().ToString() == "'")
						stack.Pop();
				}
				
				if(i == intIndex)
				{
					if(stack.Count == 0)
						return true;
					else
						return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Returns true if there is a matching string found that is not within parentheses or quotes
		/// </summary>
		public static bool IsOutsideQuotesAndParens(string str, string strToFind)
		{
			if(str.Length < 1)
				return false;

			// parentheses and quotes can be nested but never only partially overlap
			// need to be careful of something like: (param1, "(", param3)
			Stack stack = new Stack();
			Stack stackQuotes = new Stack();
			StringBuilder sb = new StringBuilder(str.Length);

			for(int i=0; i < str.Length; i++)
			{
				if(str[i] == '(' && stackQuotes.Count == 0)
					stack.Push('(');
				else if(str[i] == ')' && stackQuotes.Count == 0)
					stack.Pop();
				else if(str[i] == '"')
				{
					if(stackQuotes.Count == 0)
						stackQuotes.Push('"');
					else if(stackQuotes.Peek().ToString() == "\"")
						stackQuotes.Pop();
				}
				else if(str[i] == '\'')
				{
					if(stackQuotes.Count == 0)
						stackQuotes.Push('\'');
					else if(stackQuotes.Peek().ToString() == "'")
						stackQuotes.Pop();
				}
				else if(stack.Count == 0 && stackQuotes.Count == 0)
					sb.Append(str[i]);
			}

			return (sb.ToString().IndexOf(strToFind) >= 0) ? true : false;
		}

		public static String LikeStrToRegEx(String strLikeStr)
		{
			String strRet;
			strRet = Regex.Escape(strLikeStr); //replaces certain strings with their escape codes
			
			strRet = strRet.Replace("#", "[0-9]");		
			strRet = strRet.Replace("\\#", "[0-9]");
			strRet = strRet.Replace("\\[", "[");
			strRet = "^" + strRet.Replace("\\*", ".*").Replace("\\?", ".") + "$";
			return strRet;
		}

		public static String Left(String str, int intLength)
		{
			if(intLength <= 0)
			{
				return "";
			}
			
			if(str.Length > intLength)
			{
				return str.Substring(0, intLength);
			}
			else
			{
				return str;
			}
		}
		
		public static bool LikeStr(String strToCheck, String strLike)
		{
			//regular expressions
			//isalpha ("[^a-zA-Z]");
			//isalphanumeric ("[^a-zA-Z0-9]");
			//isnumber ("[^0-9]");

			strLike = LikeStrToRegEx(strLike);
			Regex objPositivePattern = new Regex(strLike, RegexOptions.IgnoreCase);
			return objPositivePattern.IsMatch(strToCheck);
			// could do instead: Regex.IsMatch(strToCheck, strLike, RegexOptions.IgnoreCase);
		}
		
		public static String Mid(String strValue, int intStart)
		{
			return Mid(strValue, intStart, -1);
		}

		public static String Mid(String strValue, int intStart, int intLen)
		{
			if(intStart < 1)
			{
				intStart = 1;
			}
			if(intStart > strValue.Length)
			{
				return "";
			}
			if(intLen==0)
			{
				return "";
			}
			else if(intStart - 1 + intLen > strValue.Length)
			{
				return strValue.Substring(intStart - 1);
			}
			else if(intLen > 0)
			{
				return strValue.Substring(intStart - 1, intLen);
			}
			else
			{	//intLen < 0.. return the whole rest of the string
				return strValue.Substring(intStart - 1);
			}
		}
		
		/// <summary>
		/// strname must look like Jones, Tom or Jones Tom M
		/// </summary>
		/// <param name="strName"></param>
		/// <returns></returns>
		public static String NameGrabLastName(String strName)
		{
			//'strname must look like this:
			//'jones, tom j
			//'or jones tom m

			int intPos;
			String strRet = "";
			strName = strName.Trim();

			intPos = UtilString.InStr(1, strName, ",");
			if(intPos > 1)
			{
				strRet = UtilString.Mid(strName, 1, intPos - 1).Trim();
			}
			else if(intPos == 0)
			{
				intPos = UtilString.InStr(1, strName, " ");

				if(intPos > 1)
				{
					strRet = UtilString.Mid(strName, 1, intPos - 1).Trim();
				}
				else if(intPos == 0)
				{
					strRet = strName.Trim();
				}
				else if(intPos == 1)
				{
					strRet = "";
				}
			}
			else if(intPos == 1)
			{
				strRet = "";
			}
			return strRet;
		}

		/// <summary>
		/// strname must look like Jones, Tom or Jones Tom M (then it will return 'Tom M')
		/// </summary>
		/// <param name="strName"></param>
		/// <returns></returns>
		public static String NameGrabFirstName(String strName)
		{
			//'strname must look
			//'or jones tom m
			String strRet = "";
			int intPos;
			intPos = InStr(1, strName, ",");
			if(intPos == 0)
			{
				intPos = InStr(1, strName, " ");
				
				if(intPos == 0)
				{
					strRet = "";
				}
				else if(intPos < strName.Length)
				{
					strRet = UtilString.Mid(strName, intPos + 1).Trim();
				}
				else if(intPos == strName.Length)
				{
					strRet = "";
				}
			}
			else if(intPos < strName.Length)
			{
				strRet = UtilString.Mid(strName, intPos + 1).Trim();
			}
			else if(intPos == strName.Length)
			{
				strRet = "";
			}
			return strRet;
		}
		
		/// <summary>
		/// take spaces out of the name before the comma so last names dont get split up 
		/// ie: MC DONNELL, CAROL   or  DE ANGELO, RIVERA
		/// </summary>
		/// <param name="strName"></param>
		/// <returns></returns>
		public static String NameMergeBeforeComma(String strName)
		{
			//'take spaces out of the name before the comma so last names dont get split up
			//' ie: MC DONNELL, CAROL   or  DE ANGELO, RIVERA

			int intFirst;
			int intFirstSpace;
			String retVal;

			intFirst = UtilString.InStr(1, strName, ",");
			intFirstSpace = UtilString.InStr(1, strName, " ");

			//'dont want to merge anything if the comma is the last character
			if((strName.Length != intFirst) && (intFirst > 1))
			{
				if(intFirstSpace < intFirst && intFirstSpace < 4)
				{
					strName = UtilString.Replace(UtilString.Mid(strName, 1, intFirst - 1), " ", "") + UtilString.Mid(strName, intFirst);
				}
			}

			retVal = strName;
			return retVal;
		}
		
		/// <summary>
		/// this function puts bars inbetween the last, first and middle names;
		/// name should be in this format: LAST, FIRST MIDDLE 
		/// or LAST FIRST MIDDLE;
		/// Output looks like this: 'SMITH|JOHN|R.'
		/// </summary>
		/// <param name="strName"></param>
		/// <param name="strDelim"></param>
		/// <returns></returns>
		public static String NameSplit(String strName, String strDelim)
		{
			int firstDelimPos;
			int secondDelimPos;
			int i;
			int length;
			String tempChar;
			String retVal;

			firstDelimPos = -1;
			secondDelimPos = -1;
			length = strName.Length;

			//'this is here because names often come in like ' MC CALLAN, DONNA' and
			//' if we didn't call it, this function would output MC|CALLAN|DONNA
			strName = NameMergeBeforeComma(strName);

			strName = UtilString.Replace(strName, ",", " ");
			strName = ReplaceExtraSpaces(strName);

			for(i = 1; i <= length; i++)
			{
				tempChar = UtilString.Mid(strName, i, 1);
				if(tempChar == " ")
				{
					firstDelimPos = i;
					strName = UtilString.Mid(strName, 1, i - 1) + strDelim + UtilString.Mid(strName, i + 1);
					break;
				}
			}

			//'now reverse from the end
			for(i = length; i >=1; i--)
			{
				tempChar = UtilString.Mid(strName, i, 1);
				if(tempChar == " ")
				{
					secondDelimPos = i;
			        
					if(i <= firstDelimPos)
					{
						break;
					}
			        
					strName = UtilString.Mid(strName, 1, i - 1) + strDelim + UtilString.Mid(strName, i + 1);
			         
					//'if there is another space following, take it out out
					if(UtilString.Mid(strName, i + 1, 1) == " ")
					{
						strName = UtilString.Mid(strName, 1, i) + UtilString.Mid(strName, i + 2);
					}
					break;
				}
			}
			  
			//'there HAS to be 2 delimiters in this string at the end of this function
			while(Count(strName, strDelim) < 2)
			{
				strName = strName + strDelim;
			}

			retVal = strName;
			return retVal;
		}

		public static string OCRFixAlpha(string strString)
		{
			strString = strString.Replace(" ", "");
			strString = strString.Replace(".", "");
			strString = strString.Replace(",", "");
			strString = strString.Replace(":", "");
			strString = strString.Replace("!", "");
			strString = strString.Replace("?", "");
			strString = strString.Replace("'", "");
			strString = strString.Replace("+", "");
			strString = strString.Replace(";", "");
			strString = strString.Replace("`", "");
			strString = strString.Replace("-", "");
			strString = strString.Replace("_", "");
			strString = strString.Replace("(", "");
			strString = strString.Replace(")", "");
			strString = strString.Replace("/", "");
			strString = strString.Replace("\\", "");
			strString = strString.Replace(">", "");
			strString = strString.Replace("<", "");
			strString = strString.Replace("1", "I");
			strString = strString.Replace("2", "Z");
			strString = strString.Replace("3", "B");
			strString = strString.Replace("4", "H");
			strString = strString.Replace("5", "S");
			strString = strString.Replace("6", "G");
			strString = strString.Replace("7", "T");
			strString = strString.Replace("8", "B");
			strString = strString.Replace("9", "");
			strString = strString.Replace("0", "O");
			strString = strString.Replace("|", "L");
			return strString;
		}

		public static string OCRCleanAlpha(string str)
		{
			//clean up the string because it just came out of OCR.. it should only be letters

			str = str.ToUpper();
			//remove credentials, punctuation, spaces and fix number errors ie 0's change to O's
			str = ReplacePunct(str);
			str = OCRFixAlpha(str);
			return str;
		}

		public static string OCRCleanNumber(string strNum)
		{
			//clean up the number because it just came out of OCR ..
			// make guesses at what the numbers should be

			strNum = strNum.ToUpper();
			//replace O's with 0's then remove non integers
			strNum = strNum.Replace("O", "0");
			strNum = strNum.Replace("I", "1");
			strNum = strNum.Replace("Z", "2");
			strNum = strNum.Replace("D", "0");
			strNum = strNum.Replace("G", "6");
			strNum = strNum.Replace("T", "7");
			strNum = strNum.Replace("B", "8");
			strNum = strNum.Replace("H", "4");
			strNum = strNum.Replace("|", "1");
			strNum = strNum.Replace("L", "1");
			strNum = strNum.Replace("S", "5");

			strNum = ReplaceNonInt(strNum, "", true, true);
			return strNum;
		}

		/// <summary>
		/// Remove outer quotes if present
		/// </summary>
		public static string RemoveOuterQuotes(string str)
		{
			if( (str.StartsWith("\"") && str.EndsWith("\"")) || (str.StartsWith("'") && str.EndsWith("'")) )
				str = (str.Length == 2) ? "" : str.Substring(1, str.Length - 2);
			return str;
		}

		public static String ReplaceExtraSpaces(String str)
		{
			while(str.IndexOf("  ", 0) > -1)
			{
				str = str.Replace("  ", " ");
			}
			return str;
		}
		
		public static String ReplacePunct(String str) { return ReplacePunct(str, ""); }
		public static String ReplacePunct(String str, String strReplaceWith)
		{
			String strPunctList = CharListPunct();
			str = Replace(str, strPunctList, strReplaceWith);
			return str;
		}
		
		public static String ReplaceInt(String str, String strReplaceWith)
		{
			String strList = CharListInt(false, false);
			str = Replace(str, strList, strReplaceWith);
			return str;
		}
		
		public static String ReplaceNonInt(String str, String strReplaceWith, bool ReplaceDec, bool ReplaceNeg)
		{
			String strList = CharListInt(!ReplaceDec, !ReplaceNeg);
			str = Replace(str, strList, strReplaceWith, true);
			return str;
		}
		
		public static String ReplaceNonAlpha(String str, String strReplaceWith)
		{
			String strList = CharListAlpha();
			str = Replace(str, strList, strReplaceWith, true);
			return str;
		}

		public static String ReplaceWild(String strOrig, String strFind, String strInput)
		{
			string strTemp = "";
			string strFixed = "";

			if(InStr(1, strFind, "*") > 0) // can't process asterisks, allows improper # of chars
				return "";

			for(int i=0; i < strOrig.Length; i++)
			{
				strTemp += Mid(strOrig, i, 1);
				if(strTemp.Length > strFind.Length)
				{
					strFixed += Mid(strTemp, 1, 1);
					strTemp = Mid(strTemp, 2);
				}
				if(LikeStr(strTemp, strFind))
				{
					strTemp = "";
					strFixed += strInput;
				}
			}

			strFixed += strTemp; // put on any stragglers

			return strFixed;
		}

		public static String Replace(String str, String strCharList, String strReplaceWith)
		{
			return Replace(str, strCharList, strReplaceWith, false);
		}

		public static String Replace(String str, String strCharList, String strReplaceWith, bool boolInverse)
		{
			//for just a regular replace function (not a char list, but full strings - use str.Replace)

			//strReplaceChars is a list of characters to replace ie "1234567890" .. would emulate a "replaceint" function
			//                     (unless boolInverse was set to true, then it would emulate a "replacenonint" function
			//strReplaceWith : a string to replace each character in the character list with (this can be any length)
			//boolInverse: if set to false, all characters in the charlist will be replaced, otherwise, everything else
			// will be replaced
			
			int i, c;
			if(str.Length == 0)
			{
				return "";
			}
			
			c = str.Length;
			for(i=c-1; i>=0; i--)
			{
				if(boolInverse)
				{
					if(strCharList.IndexOf(str.Substring(i, 1)) < 0)
					{
						str = str.Remove(i, 1);
						str = str.Insert(i, strReplaceWith);
					}
				}
				else
				{
					if(strCharList.IndexOf(str.Substring(i, 1)) >= 0)
					{
						str = str.Remove(i, 1);
						str = str.Insert(i, strReplaceWith);
					}
				}
			}

			return str;
		}
		
		public static String ReplaceByIndex(String str, String strReplaceWith, int intStart, int intEnd)
		{
			// intStart and intEnd are inclusive
			string strRet = "";

			if(intStart > 0)
				strRet = str.Substring(0, intStart);

			strRet += strReplaceWith;
			strRet += str.Substring(intEnd+1);

			return strRet;
		}
		
		public static String Right(String str, int intLength)
		{
			if(intLength <= 0)
			{
				return "";
			}
			if(str.Length > intLength)
			{
				return str.Substring(str.Length - intLength);
			}
			else
			{
				return str;
			}
		}
		
		public static void Run(String strCommand, String strArguments)
		{
			//System.Diagnostics.Process prcNew = new System.Diagnostics.Process();
			
			System.Diagnostics.Process.Start(strCommand, strArguments);
			//if(boolWaitFor)
			//{
			//	prcNew.WaitForExit();
			//}
		}
		
		public static ArrayList Split(String str, String split)
		{
			//return an array of strings
			ArrayList arrRet = new ArrayList();
			arrRet.Clear();
	
			String strTemp;
			int i, intLen;
			int intTemp1;
			intLen = str.Length; 
			int intLenSplit;
			intLenSplit = split.Length;

			if(intLen==0 || intLenSplit==0)
			{
				return arrRet;
			}

			int intCount = Count(str, split);
			if(intCount==0)
			{
				arrRet.Add(str);
				return arrRet;
			}

			i = 0;
			while(i <= intLen)
			{
				intTemp1 = str.IndexOf(split, i);
				if(intTemp1<0)
				{
					strTemp = str.Substring(i, intLen - i);
					arrRet.Add(strTemp);
					return arrRet;
				}
				else
				{
					strTemp = str.Substring(i, intTemp1 - i); 
					arrRet.Add(strTemp);
					i = intTemp1 + intLenSplit;
				}
			}
			return arrRet;
		}

		/// <summary>
		/// Splits a string, but does not split anything inside parentheses
		/// </summary>
		public static ArrayList SplitOutsideParentheses(string str, string strSplit)
		{ return SplitOutsideParentheses(str, strSplit, null); }
		public static ArrayList SplitOutsideParentheses(string str, char[] arrChar)
		{ return SplitOutsideParentheses(str, null, arrChar); }
		private static ArrayList SplitOutsideParentheses(string str, string strSplit, char[] arrChar)
		{
			//return an array of strings
			ArrayList arrRet = new ArrayList();

			if(str.Length == 0)
			{
				arrRet.Add("");
				return arrRet;
			}

			// remove/save everything in parentheses
			int intSave = -1;
			int intStart = -1;
			int intLength = 0;
			string strNew = "";

			Stack stack = new Stack();
			ArrayList arrSave = new ArrayList();

			char chTemp;
			for(int i=0; i < str.Length; i++)
			{
				chTemp = str[i];

				if(chTemp == '(')
				{
					stack.Push(chTemp);
					if(intSave == -1)
						intSave = i;
				}
				else if(chTemp == ')')
				{
					stack.Pop();
					if(stack.Count == 0)
					{
						intStart = intSave + 1;
						intLength = i - intSave - 1;
						if(intLength > 0)
							arrSave.Add(str.Substring(intStart, intLength));
						else
							arrSave.Add("");
						strNew += "()";

						intSave = -1;
					}
				}
				else if(stack.Count == 0)
				{
					strNew += str[i];
				}
			}

			// now do normal splitting
			if(strSplit != null)
				arrRet = UtilString.Split(strNew, strSplit);
			else
				arrRet = new ArrayList(strNew.Split(arrChar));

			// now we fill the parentheses back in
			int intTemp = 0;
			int intSavePos = 0;
			intStart = -1;

			for(int i=0; i < arrRet.Count; i++)
			{
				while( (intStart = arrRet[i].ToString().IndexOf("()", intTemp)) >= 0)
				{
					arrRet[i] = UtilString.ReplaceByIndex(arrRet[i].ToString(), "(" + arrSave[intSavePos].ToString() + ")", intStart, intStart + 1);
					intTemp = intStart + arrSave[intSavePos].ToString().Length + 2;
					intSavePos++;
				}

				intTemp = 0;
			}
			return arrRet;
		}

		/// <summary>
		/// Splits a string, but does not split anything inside quotes
		/// </summary>
		public static ArrayList SplitOutsideQuotes(string str, string strSplit)
		{ return SplitOutsideQuotes(str, strSplit, null); }
		public static ArrayList SplitOutsideQuotes(string str, char[] arrChar)
		{ return SplitOutsideQuotes(str, null, arrChar); }
		private static ArrayList SplitOutsideQuotes(string str, string strSplit, char[] arrChar)
		{
			//return an array of strings
			ArrayList arrRet = new ArrayList();

			if(str.Length == 0)
			{
				arrRet.Add("");
				return arrRet;
			}

			// remove/save everything in quotes
			int intSave = -1;
			int intStart = -1;
			int intLength = 0;
			string strNew = "";

			Stack stackQuotes = new Stack();
			ArrayList arrSaveSingleQuotes = new ArrayList();
			ArrayList arrSaveDoubleQuotes = new ArrayList();

			char chTemp;
			for(int i=0; i < str.Length; i++)
			{
				chTemp = str[i];

				if( (chTemp == '"' || chTemp == '\''))
				{
					if(stackQuotes.Count == 0)
					{
						stackQuotes.Push(chTemp);
						if(intSave == -1)
							intSave = i;
					}
					else if(chTemp == '\'' && stackQuotes.Peek().ToString() == "'")
					{
							// this is a closing single quote
							stackQuotes.Pop();

							intStart = intSave + 1;
							intLength = i - intSave - 1;
							if(intLength > 0)
								arrSaveSingleQuotes.Add(str.Substring(intStart, intLength));
							else
								arrSaveSingleQuotes.Add("");
							strNew += "(&S)";

							intSave = -1;
					}
					else if(chTemp == '"' && stackQuotes.Peek().ToString() == "\"")
					{
						// this is a closing double quote
						stackQuotes.Pop();

						intStart = intSave + 1;
						intLength = i - intSave - 1;
						if(intLength > 0)
							arrSaveDoubleQuotes.Add(str.Substring(intStart, intLength));
						else
							arrSaveDoubleQuotes.Add("");
						strNew += "(&D)";

						intSave = -1;
					}
				}
				else if(stackQuotes.Count == 0)
				{
					strNew += str[i];
				}
			}

			// now do normal splitting
			if(strSplit != null)
				arrRet = UtilString.Split(strNew, strSplit);
			else
				arrRet = new ArrayList(strNew.Split(arrChar));

			// now we fill the parentheses/quotes back in
			int intSavePosS = 0;
			int intSavePosD = 0;
			int intTemp;
			intStart = -1;

			for(int i=0; i < arrRet.Count; i++)
			{
				intTemp = 0;
				while( (intStart = arrRet[i].ToString().IndexOf("(&S)", intTemp)) >= 0)
				{
					arrRet[i] = UtilString.ReplaceByIndex(arrRet[i].ToString(), "\'" + arrSaveSingleQuotes[intSavePosS].ToString() + "\'", intStart, intStart + 3);
					intTemp = intStart + arrSaveSingleQuotes[intSavePosS].ToString().Length + 2;
					intSavePosS++;
				}
				intTemp = 0;
				while( (intStart = arrRet[i].ToString().IndexOf("(&D)", intTemp)) >= 0)
				{
					arrRet[i] = UtilString.ReplaceByIndex(arrRet[i].ToString(), "\"" + arrSaveDoubleQuotes[intSavePosD].ToString() + "\"", intStart, intStart + 3);
					intTemp = intStart + arrSaveDoubleQuotes[intSavePosD].ToString().Length + 2;
					intSavePosD++;
				}
			}
			return arrRet;
		}

		/// <summary>
		/// Splits a string, but does not split anything inside quotes or parentheses
		/// </summary>
		public static ArrayList SplitOutsideQuotesAndParens(string str, string strSplit)
		{ return SplitOutsideQuotesAndParens(str, strSplit, null); }
		public static ArrayList SplitOutsideQuotesAndParens(string str, char[] arrChar)
		{ return SplitOutsideQuotesAndParens(str, null, arrChar); }
		private static ArrayList SplitOutsideQuotesAndParens(string str, string strSplit, char[] arrChar)
		{
			// parentheses and quotes can be nested but never only partially overlap
			// need to be careful of something like: (param1, "(", param3)

			//return an array of strings
			ArrayList arrRet = new ArrayList();

			if(str.Length == 0)
			{
				arrRet.Add("");
				return arrRet;
			}

			// remove/save everything in parentheses/quotes
			int intSave = -1;
			int intStart = -1;
			int intLength = 0;
			string strNew = "";

			Stack stack = new Stack();
			Stack stackQuotes = new Stack();
			ArrayList arrSaveParens = new ArrayList();
			ArrayList arrSaveSingleQuotes = new ArrayList();
			ArrayList arrSaveDoubleQuotes = new ArrayList();

			char chTemp;
			for(int i=0; i < str.Length; i++)
			{
				chTemp = str[i];

				if(chTemp == '(' && stackQuotes.Count == 0)
				{
					if(stack.Count == 0)
					{
						stack.Push(chTemp);
						if(intSave == -1)
							intSave = i;
					}
				}
				else if( (chTemp == '"' || chTemp == '\'') && stack.Count == 0)
				{
					if(stackQuotes.Count == 0)
					{
						stackQuotes.Push(chTemp);
						if(intSave == -1)
							intSave = i;
					}
					else
					{
						if(chTemp == '\'' && stackQuotes.Peek().ToString() == "'")
						{
							// this is a closing single quote
							stackQuotes.Pop();

							intStart = intSave + 1;
							intLength = i - intSave - 1;
							if(intLength > 0)
								arrSaveSingleQuotes.Add(str.Substring(intStart, intLength));
							else
								arrSaveSingleQuotes.Add("");
							strNew += "(&S)";

							intSave = -1;
						}
						else if(chTemp == '"' && stackQuotes.Peek().ToString() == "\"")
						{
							// this is a closing double quote
							stackQuotes.Pop();

							intStart = intSave + 1;
							intLength = i - intSave - 1;
							if(intLength > 0)
								arrSaveDoubleQuotes.Add(str.Substring(intStart, intLength));
							else
								arrSaveDoubleQuotes.Add("");
							strNew += "(&D)";

							intSave = -1;
						}
					}
				}
				else if(chTemp == ')' && stack.Count > 0)
				{
					stack.Pop();
					if(stack.Count == 0)
					{
						intStart = intSave + 1;
						intLength = i - intSave - 1;
						if(intLength > 0)
							arrSaveParens.Add(str.Substring(intStart, intLength));
						else
							arrSaveParens.Add("");
						strNew += "(&P)";

						intSave = -1;
					}
				}
				else if(stack.Count == 0 && stackQuotes.Count == 0)
				{
					strNew += str[i];
				}
			}

			// now do normal splitting
			if(strSplit != null)
				arrRet = UtilString.Split(strNew, strSplit);
			else
				arrRet = new ArrayList(strNew.Split(arrChar));

			// now we fill the parentheses/quotes back in
			int intSavePosS = 0;
			int intSavePosD = 0;
			int intSavePosP = 0;
			int intTemp;
			intStart = -1;

			for(int i=0; i < arrRet.Count; i++)
			{
				intTemp = 0;
				while( (intStart = arrRet[i].ToString().IndexOf("(&S)", intTemp)) >= 0)
				{
					arrRet[i] = UtilString.ReplaceByIndex(arrRet[i].ToString(), "\'" + arrSaveSingleQuotes[intSavePosS].ToString() + "\'", intStart, intStart + 3);
					intTemp = intStart + arrSaveSingleQuotes[intSavePosS].ToString().Length + 2;
					intSavePosS++;
				}
				intTemp = 0;
				while( (intStart = arrRet[i].ToString().IndexOf("(&D)", intTemp)) >= 0)
				{
					arrRet[i] = UtilString.ReplaceByIndex(arrRet[i].ToString(), "\"" + arrSaveDoubleQuotes[intSavePosD].ToString() + "\"", intStart, intStart + 3);
					intTemp = intStart + arrSaveDoubleQuotes[intSavePosD].ToString().Length + 2;
					intSavePosD++;
				}
				intTemp = 0;
				while( (intStart = arrRet[i].ToString().IndexOf("(&P)", intTemp)) >= 0)
				{
					arrRet[i] = UtilString.ReplaceByIndex(arrRet[i].ToString(), "(" + arrSaveParens[intSavePosP].ToString() + ")", intStart, intStart + 3);
					intTemp = intStart + arrSaveParens[intSavePosP].ToString().Length + 2;
					intSavePosP++;
				}
			}
			return arrRet;
		}

		public static int StringCompare(String strOne, String strTwo)
		{
			ArrayList arrCompOne = new ArrayList();
			ArrayList arrCompTwo = new ArrayList();
			int countX;
			int countY;
			int countI;
			int countJ;
			int countMatch;
			int lngTotalMatch = 0;
			int countLetter;

			strOne = strOne.ToUpper();
			strTwo = strTwo.ToUpper();
			if (strOne == strTwo)
			{
				return 1000;
			}

			if (strOne.Length == 0 || strTwo.Length == 0)
			{ return 0; }

			for (countLetter = 0; countLetter < strOne.Length; countLetter++)
			{
				arrCompOne.Add(strOne.Substring(countLetter, 1));
			}
			for (countLetter = 0; countLetter < strTwo.Length; countLetter++)
			{
				arrCompTwo.Add(strTwo.Substring(countLetter, 1));
			}

			//'code for fuzzy logic string comparison found at:
			//'http://www.english.upenn.edu/~jlynch/Computing/compare.html
			for (countI = 0; countI < strOne.Length; countI++)
			{
				countX = countI;
				for (countJ = 0; countJ < strTwo.Length; countJ++)
				{
					countY = countJ;
					countMatch = 0;
					while ((countX < strOne.Length) && (countY < strTwo.Length)
						&& (arrCompOne[countX].ToString() == arrCompTwo[countY].ToString()))
					{
						countMatch++;
						lngTotalMatch += countMatch * countMatch;
						countX++;
						countY++;
					}
				}
			}

			lngTotalMatch = (lngTotalMatch * 25) / (strOne.Length * strTwo.Length);

			if (lngTotalMatch > 1000)
			{
				lngTotalMatch = 1000;
			}

			return lngTotalMatch;
		}
		
		public static int CompareNamePart(string strPart, string strName)
		{
			return CompareNamePart(strPart, strName, 500);
		}
		/// <summary>
		/// looks to see if a part of a name can find a match in the full name 
		///  -- not sure where the part of the name will be in the string, so have to check strPart vs every word in strName
		/// </summary>
		/// <returns>a comparison value, from UtilString.StringCompare, just searches around for the part of the name</returns>
		public static int CompareNamePart(string strPart, string strName, int intMax)
		{
			string strChar;
			string strCheck;
			string strWord;
			int intCompare;
			int intBest;
			string strBest;
			string strTest1;
			string strTest2;
			string strPartFix;

			strCheck = strName.Trim() + " ";
			//space on end marks end of last word
			intBest = 0;
			strBest = "";
			strWord = "";
    
			if ((strPart == "") || (strName == "")) 
			{
				//do not want to return false matches for blanks
				return 0;
			}
    
			strPartFix = UtilString.ReplacePunct(strPart, " ");
			strCheck = UtilString.ReplacePunct(strCheck, " ");
			while (UtilString.LikeStr(strPartFix, "*  *"))
			{
				strPartFix = strPartFix.Replace("  ", " ");
			}
			while (UtilString.LikeStr(strCheck, "*  *"))
			{
				strCheck = strCheck.Replace("  ", " ");
			}
    
			for (int intX = 1; intX <= strCheck.Length; intX++) 
			{
				strChar = UtilString.Mid(strCheck, intX, 1);
				if (strChar == " ") 
				{
					//we hit the end of a word
					//wanna compare words at same length, to help with ANN vs ANNMARIE situations
					if (strPartFix.Length > strWord.Length) 
					{
						strTest1 = strWord;
						strTest2 = UtilString.Mid(strPartFix, 1, strWord.Length);
					}
					else 
					{
						strTest1 = strPartFix;
						strTest2 = UtilString.Mid(strWord, 1, strPartFix.Length);
					}
					intCompare = UtilString.StringCompare(strTest1, strTest2);
					if (((strWord.Length == 1) || (strPart.Length == 1)) && (strWord.Length != strPart.Length) && (intCompare == 1000)) 
					{
						//do not want to overvalue single letters, especially when both words are not just 1 letter!
						intCompare = 10;
					}
					else if ((strTest1.Length == 1) & (intCompare == 1000)) 
					{
						//even if both are 1 letter, don't overvalue
						intCompare = 100;
					}
					if (intCompare > intBest) 
					{
						intBest = intCompare;
						strBest = strWord;
					}
					strWord = "";
				}
				else 
				{
					strWord = strWord + strChar;
				}
			}
    
			if (intBest > intMax) 
			{
				intBest = intMax; //reduce size of perfect match to make partial values easier to handle for long names
			}
			return intBest;
		}

		public static int CompareNamePieces(string strOne, string strTwo)
		{
			int functionReturnValue = 0;
			//goes through Name1, splits it into separate words, gets a confidence value on finding each word in Name2
			//reduces the value of the pieces to prevent mismatches
			//goals:
			//  -reduce the value of shorter words significantly
			//  -reduce the values returned for names as the number of words increases
			//   (don't want to artificially inflate the value of an amount because there was a whole bunch of words)
			//   ^- no longer doing that
    
			//returns:
			//  100+ for a great match
			//   75  for a good match
			//   70  for an okay match
			//   65  for a shaky match
			//   60- is really sketchy
			string strWord;
			int intWords;
			string strName1;
			string strName2;
			int intX;
			string strChar;
			int intCompare;
			int intValue;
    
			strName1 = UtilString.ReplaceCred(strOne).Trim().ToUpper();
			strName1 = UtilString.ReplaceNameAbbrev(strName1).Trim().ToUpper();
			strName1 = UtilString.ReplacePunct(strName1, " ") + " ";
			strName1 = UtilString.ReplaceExtraSpaces(strName1);
    
			strName2 = UtilString.ReplaceCred(strTwo).Trim().ToUpper();
			strName2 = UtilString.ReplaceNameAbbrev(strName2).Trim().ToUpper();
			strName2 = UtilString.ReplacePunct(strName2, " ") + " ";
			strName2 = UtilString.ReplaceExtraSpaces(strName2);
    
			if (strName1 == strName2) 
			{
				return 1000;
			}
    
			strWord = "";
			intWords = 0;
			intCompare = 0;
			intValue = 0;
			for (intX = 1; intX <= strName1.Length; intX++) 
			{
				System.Windows.Forms.Application.DoEvents();
        
				strChar = UtilString.Mid(strName1, intX, 1);
				if (strChar == " ") 
				{
					intWords = intWords + 1;
            
					if (strWord.Length > 5) 
					{
						//longer than 5 chars, 120 = value of perfect match; for 2 words, 120 (1 word match) returns 40
						intValue = UtilString.CompareNamePart(strWord, strName2, 120);
					}
					else if (strWord.Length > 2) 
					{
						//2-9 chars, value of perfect match = 20 * length
						intValue = UtilString.CompareNamePart(strWord, strName2, (20 * strWord.Length));
					}
					else if (strWord.Length > 0) 
					{
						//really short words should count even less, 10 * length
						intValue = UtilString.CompareNamePart(strWord, strName2, (10 * strWord.Length));
						if ((strWord.Length == 1) && (intWords > 1)) 
						{
							//1 letter words should not hurt the total value
							intWords = intWords - 1;
						}
					}
					else 
					{
						//not a word
						intWords = intWords - 1;
					}
            
					intCompare = intCompare + intValue;
            
					strWord = "";
				}
				else 
				{
					strWord = strWord + strChar;
				}
        
			}
    
			if (intWords == 0) 
			{
				functionReturnValue = 0;
			}
			else if (intWords == 1) 
			{
				//don't want it to be overvalued as a one word match
				if (intCompare > 50) 
				{
					functionReturnValue = 50;
				}
				else 
				{
					functionReturnValue = intCompare;
				}
			}
			else 
			{
				//get the average of all words, this means max is 250
				//(all words over 9 chars long, all words perfect match)
				//divide by (word count * 1.5) to give strings with more words a penalty for having more chances to match
				//CompareNamePieces = intCompare \ (intWords * 1.5)
				//now trying just a straight up average....
				functionReturnValue = intCompare / intWords;
			}
			return functionReturnValue;
		}

		public static string ReplaceCred(string strValue)
		{
			return ReplaceCred(strValue, " ");
		}
		public static string ReplaceCred(string strValue, string strReplace)
		{
			string strVal;
			string strRep;
    
			if (strReplace != " ") 
			{
				strRep = strReplace + " ";
			}
			else 
			{
				strRep = " ";
			}
			strVal = " " + strValue.Trim().ToUpper() + " ";
			//add space on ends to assist with replaces
    
			//comma ones must come first!
    
			strVal = strVal.Replace(",ACSW ", strRep).Replace(", ACSW ", strRep).Replace(" ACSW ", strRep);
    
			strVal = strVal.Replace(",ARNP ", strRep).Replace(", ARNP ", strRep).Replace(" ARNP ", strRep);
    
			strVal = strVal.Replace(",AST ", strRep).Replace(", AST ", strRep).Replace(" AST ", strRep);
			strVal = strVal.Replace(",AST. ", strRep).Replace(", AST. ", strRep).Replace(" AST. ", strRep);
    
			strVal = strVal.Replace(",CNM ", strRep).Replace(", CNM ", strRep).Replace(" CNM ", strRep);
    
			strVal = strVal.Replace(",CPO ", strRep).Replace(", CPO ", strRep).Replace(" CPO ", strRep);
    
			strVal = strVal.Replace(",CP ", strRep).Replace(", CP ", strRep).Replace(" CP ", strRep);
    
			strVal = strVal.Replace(",CRNA ", strRep).Replace(", CRNA ", strRep).Replace(" CRNA ", strRep);
    
			strVal = strVal.Replace(",DC ", strRep).Replace(", DC ", strRep).Replace(" DC ", strRep);
			strVal = strVal.Replace(",D.C. ", strRep).Replace(", D.C. ", strRep).Replace(" D.C. ", strRep);
			strVal = strVal.Replace(",DC. ", strRep).Replace(", DC. ", strRep).Replace(" DC. ", strRep);
			strVal = strVal.Replace(",D.C ", strRep).Replace(", D.C ", strRep).Replace(" D.C ", strRep);
			strVal = strVal.Replace(",D C. ", strRep).Replace(", D C. ", strRep).Replace(" D C. ", strRep);
			strVal = strVal.Replace(",D C ", strRep).Replace(", D C ", strRep).Replace(" D C ", strRep);
			strVal = strVal.Replace(",D. C. ", strRep).Replace(", D. C. ", strRep).Replace(" D. C. ", strRep);
    
			strVal = strVal.Replace(",DDS ", strRep).Replace(", DDS ", strRep).Replace(" DDS ", strRep);
			strVal = strVal.Replace(",DDS. ", strRep).Replace(", DDS. ", strRep).Replace(" DDS. ", strRep);
			strVal = strVal.Replace(",D.D.S. ", strRep).Replace(", D.D.S. ", strRep).Replace(" D.D.S. ", strRep);
			strVal = strVal.Replace(",D D S ", strRep).Replace(", D D S ", strRep).Replace(" D D S ", strRep);
			strVal = strVal.Replace(",D. D. S. ", strRep).Replace(", D. D. S. ", strRep).Replace(" D. D. S. ", strRep);
    
			strVal = strVal.Replace(",DIR ", strRep).Replace(", DIR ", strRep).Replace(" DIR ", strRep);
			strVal = strVal.Replace(",DIR. ", strRep).Replace(", DIR. ", strRep).Replace(" DIR. ", strRep);
    
			strVal = strVal.Replace(",DMA ", strRep).Replace(", DMA ", strRep).Replace(" DMA ", strRep);
    
			strVal = strVal.Replace(",DMD ", strRep).Replace(", DMD ", strRep).Replace(" DMD ", strRep);
    
			strVal = strVal.Replace(",DO ", strRep).Replace(", DO ", strRep).Replace(" DO ", strRep);
			strVal = strVal.Replace(",D.O. ", strRep).Replace(", D.O. ", strRep).Replace(" D.O. ", strRep);
			strVal = strVal.Replace(",DO. ", strRep).Replace(", DO. ", strRep).Replace(" DO. ", strRep);
			strVal = strVal.Replace(",D.O ", strRep).Replace(", D.O ", strRep).Replace(" D.O ", strRep);
			strVal = strVal.Replace(",D O. ", strRep).Replace(", D O. ", strRep).Replace(" D O. ", strRep);
			strVal = strVal.Replace(",D O ", strRep).Replace(", D O ", strRep).Replace(" D O ", strRep);
			strVal = strVal.Replace(",D. O. ", strRep).Replace(", D. O. ", strRep).Replace(" D. O. ", strRep);
    
			strVal = strVal.Replace(",DPM ", strRep).Replace(", DPM ", strRep).Replace(" DPM ", strRep);
    
			strVal = strVal.Replace(",DR ", strRep).Replace(", DR ", strRep).Replace(" DR ", strRep);
			strVal = strVal.Replace(",DR. ", strRep).Replace(", DR. ", strRep).Replace(" DR. ", strRep);
    
			strVal = strVal.Replace(",EMBS ", strRep).Replace(", EMBS ", strRep).Replace(" EMBS ", strRep);
    
			strVal = strVal.Replace(",FAAP ", strRep).Replace(", FAAP ", strRep).Replace(" FAAP ", strRep);
			strVal = strVal.Replace(",F.A.A.P. ", strRep).Replace(", F.A.A.P. ", strRep).Replace(" F.A.A.P. ", strRep);
			strVal = strVal.Replace(",FAAP. ", strRep).Replace(", FAAP. ", strRep).Replace(" FAAP. ", strRep);
			strVal = strVal.Replace(",F.A.A.P ", strRep).Replace(", F.A.A.P ", strRep).Replace(" F.A.A.P ", strRep);
			strVal = strVal.Replace(",F A A P ", strRep).Replace(", F A A P ", strRep).Replace(" F A A P ", strRep);
    
			strVal = strVal.Replace(",FP ", strRep).Replace(", FP ", strRep).Replace(" FP ", strRep);
			strVal = strVal.Replace(",FP. ", strRep).Replace(", FP. ", strRep).Replace(" FP. ", strRep);
			strVal = strVal.Replace(",F.P ", strRep).Replace(", F.P ", strRep).Replace(" F.P ", strRep);
			strVal = strVal.Replace(",F.P. ", strRep).Replace(", F.P. ", strRep).Replace(" F.P. ", strRep);
			strVal = strVal.Replace(",F P. ", strRep).Replace(", F P. ", strRep).Replace(" F P. ", strRep);
			strVal = strVal.Replace(",F P ", strRep).Replace(", F P ", strRep).Replace(" F P ", strRep);
    
			strVal = strVal.Replace(",FAGD ", strRep).Replace(", FAGD ", strRep).Replace(" FAGD ", strRep);
    
			strVal = strVal.Replace(",FC ", strRep).Replace(", FC ", strRep).Replace(" FC ", strRep);
			strVal = strVal.Replace(",FC. ", strRep).Replace(", FC. ", strRep).Replace(" FC. ", strRep);
    
			strVal = strVal.Replace(",FNP ", strRep).Replace(", FNP ", strRep).Replace(" FNP ", strRep);
    
			strVal = strVal.Replace(",INC ", strRep).Replace(", INC ", strRep).Replace(" INC ", strRep);
			strVal = strVal.Replace(",INC. ", strRep).Replace(", INC. ", strRep).Replace(" INC. ", strRep);
    
			strVal = strVal.Replace(",INS ", strRep).Replace(", INS ", strRep).Replace(" INS ", strRep);
			strVal = strVal.Replace(",INS. ", strRep).Replace(", INS. ", strRep).Replace(" INS. ", strRep);
    
			strVal = strVal.Replace(",JNC ", strRep).Replace(", JNC ", strRep).Replace(" JNC ", strRep);
    
			strVal = strVal.Replace(",JR/PA ", strRep).Replace(", JR/PA ", strRep).Replace(" JR/PA ", strRep);
    
			strVal = strVal.Replace(",LAC ", strRep).Replace(", LAC ", strRep).Replace(" LAC ", strRep);
			strVal = strVal.Replace(",LAC. ", strRep).Replace(", LAC. ", strRep).Replace(" LAC. ", strRep);
    
			strVal = strVal.Replace(",LCSW ", strRep).Replace(", LCSW ", strRep).Replace(" LCSW", strRep);
    
			strVal = strVal.Replace(",LTD ", strRep).Replace(", LTD ", strRep).Replace(" LTD ", strRep);
			strVal = strVal.Replace(",LTD. ", strRep).Replace(", LTD. ", strRep).Replace(" LTD. ", strRep);
    
			strVal = strVal.Replace("MD ", strRep);
			strVal = strVal.Replace(",MD ", strRep).Replace(", MD ", strRep).Replace(" MD ", strRep);
			strVal = strVal.Replace(" MD, ", strRep).Replace(" MD,", strRep).Replace(" MD, ", strRep);
			strVal = strVal.Replace(",M.D. ", strRep).Replace(", M.D. ", strRep).Replace(" M.D. ", strRep);
			strVal = strVal.Replace(",MD. ", strRep).Replace(", MD. ", strRep).Replace(" MD. ", strRep);
			strVal = strVal.Replace(",M.D ", strRep).Replace(", M.D ", strRep).Replace(" M.D ", strRep);
			strVal = strVal.Replace(",M D. ", strRep).Replace(", M D. ", strRep).Replace(" M D. ", strRep);
			strVal = strVal.Replace(",M D ", strRep).Replace(", M D ", strRep).Replace(" M D ", strRep);
			strVal = strVal.Replace(",M. D. ", strRep).Replace(", M. D. ", strRep).Replace(" M. D. ", strRep);
    
			strVal = strVal.Replace(",MN ", strRep).Replace(", MN ", strRep).Replace(" MN ", strRep);
    
			strVal = strVal.Replace(",MRCP ", strRep).Replace(", MRCP ", strRep).Replace(" MRCP ", strRep);
    
			strVal = strVal.Replace(",MS ", strRep).Replace(", MS ", strRep).Replace(" MS ", strRep);
    
			strVal = strVal.Replace(",MT ", strRep).Replace(", MT ", strRep).Replace(" MT ", strRep);
    
			strVal = strVal.Replace(",NP ", strRep).Replace(", NP ", strRep).Replace(" NP ", strRep);
    
			strVal = strVal.Replace(",OD ", strRep).Replace(", OD ", strRep).Replace(" OD ", strRep);
			strVal = strVal.Replace(",O.D. ", strRep).Replace(", O.D. ", strRep).Replace(" O.D. ", strRep);
			strVal = strVal.Replace(",OD. ", strRep).Replace(", OD. ", strRep).Replace(" OD. ", strRep);
			strVal = strVal.Replace(",O.D ", strRep).Replace(", O.D ", strRep).Replace(" O.D ", strRep);
			strVal = strVal.Replace(",O D. ", strRep).Replace(", O D. ", strRep).Replace(" O D. ", strRep);
			strVal = strVal.Replace(",O. D. ", strRep).Replace(", O. D. ", strRep).Replace(" O. D. ", strRep);
			strVal = strVal.Replace(",O D ", strRep).Replace(", O D ", strRep).Replace(" O D ", strRep);
    
			strVal = strVal.Replace(",OTC ", strRep).Replace(", OTC ", strRep).Replace(" OTC ", strRep);
			strVal = strVal.Replace(",O.T.C. ", strRep).Replace(", O.T.C. ", strRep).Replace(" O.T.C. ", strRep);
			strVal = strVal.Replace(",OTC. ", strRep).Replace(", OTC. ", strRep).Replace(" OTC. ", strRep);
			strVal = strVal.Replace(",O. T. C. ", strRep).Replace(", O. T. C. ", strRep).Replace(" O. T. C. ", strRep);
			strVal = strVal.Replace(",O T C ", strRep).Replace(", O T C ", strRep).Replace(" O T C ", strRep);
    
			strVal = strVal.Replace(",OTCHT ", strRep).Replace(", OTCHT ", strRep).Replace(" OTCHT ", strRep);
    
			strVal = strVal.Replace(",OTH ", strRep).Replace(", OTH ", strRep).Replace(" OTH ", strRep);
    
			strVal = strVal.Replace(",OTR ", strRep).Replace(", OTR ", strRep).Replace(" OTR ", strRep);
    
			strVal = strVal.Replace(",PA ", strRep).Replace(", PA ", strRep).Replace(" PA ", strRep);
			strVal = strVal.Replace(",P.A. ", strRep).Replace(", P.A. ", strRep).Replace(" P.A. ", strRep);
			strVal = strVal.Replace(",PA. ", strRep).Replace(", PA. ", strRep).Replace(" PA. ", strRep);
			strVal = strVal.Replace(",P.A ", strRep).Replace(", P.A ", strRep).Replace(" P.A ", strRep);
			strVal = strVal.Replace(",P A. ", strRep).Replace(", P A. ", strRep).Replace(" P A. ", strRep);
			strVal = strVal.Replace(",P. A. ", strRep).Replace(", P. A. ", strRep).Replace(" P. A. ", strRep);
			strVal = strVal.Replace(",P A ", strRep).Replace(", P A ", strRep).Replace(" P A ", strRep);
    
			strVal = strVal.Replace(",PAC ", strRep).Replace(", PAC ", strRep).Replace(" PAC ", strRep);
    
			strVal = strVal.Replace(",PC ", strRep).Replace(", PC ", strRep).Replace(" PC ", strRep);
			strVal = strVal.Replace(",P.C. ", strRep).Replace(", P.C. ", strRep).Replace(" P.C. ", strRep);
			strVal = strVal.Replace(",PC. ", strRep).Replace(", PC. ", strRep).Replace(" PC. ", strRep);
			strVal = strVal.Replace(",P.C ", strRep).Replace(", P.C ", strRep).Replace(" P.C ", strRep);
			strVal = strVal.Replace(",P C. ", strRep).Replace(", P C. ", strRep).Replace(" P C. ", strRep);
			strVal = strVal.Replace(",P. C. ", strRep).Replace(", P. C. ", strRep).Replace(" P. C. ", strRep);
			strVal = strVal.Replace(",P C ", strRep).Replace(", P C ", strRep).Replace(" P C ", strRep);
    
			strVal = strVal.Replace(",PHD ", strRep).Replace(", PHD ", strRep).Replace(" PHD ", strRep);
			strVal = strVal.Replace(",PHD. ", strRep).Replace(", PHD. ", strRep).Replace(" PHD. ", strRep);
			strVal = strVal.Replace(",PH.D. ", strRep).Replace(", PH.D. ", strRep).Replace(" PH.D. ", strRep);
			strVal = strVal.Replace(",PH.D ", strRep).Replace(", PH.D ", strRep).Replace(" PH.D ", strRep);
			strVal = strVal.Replace(",PH D. ", strRep).Replace(", PH D. ", strRep).Replace(" PH D. ", strRep);
			strVal = strVal.Replace(",PH. D ", strRep).Replace(", PH. D ", strRep).Replace(" PH. D ", strRep);
			strVal = strVal.Replace(",PH. D. ", strRep).Replace(", PH. D. ", strRep).Replace(" PH. D. ", strRep);
			strVal = strVal.Replace(",PH D ", strRep).Replace(", PH D ", strRep).Replace(" PH D ", strRep);
    
			strVal = strVal.Replace(",PSC ", strRep).Replace(", PSC ", strRep).Replace(" PSC ", strRep);
    
			strVal = strVal.Replace(",PSYD ", strRep).Replace(", PSYD ", strRep).Replace(" PSYD ", strRep);
			strVal = strVal.Replace(",PSY. D. ", strRep).Replace(", PSY. D. ", strRep).Replace(" PSY. D. ", strRep);
			strVal = strVal.Replace(",PSY.D. ", strRep).Replace(", PSY.D. ", strRep).Replace(" PSY.D. ", strRep);
    
			strVal = strVal.Replace(",PT ", strRep).Replace(", PT ", strRep).Replace(" PT ", strRep);
			strVal = strVal.Replace(",P.T. ", strRep).Replace(", P.T. ", strRep).Replace(" P.T. ", strRep);
			strVal = strVal.Replace(",PT. ", strRep).Replace(", PT. ", strRep).Replace(" PT. ", strRep);
			strVal = strVal.Replace(",P.T ", strRep).Replace(", P.T ", strRep).Replace(" P.T ", strRep);
			strVal = strVal.Replace(",P T. ", strRep).Replace(", P T. ", strRep).Replace(" P T. ", strRep);
			strVal = strVal.Replace(",P T ", strRep).Replace(", P T ", strRep).Replace(" P T ", strRep);
    
			strVal = strVal.Replace(",RNFA ", strRep).Replace(", RNFA ", strRep).Replace(" RNFA ", strRep);
			strVal = strVal.Replace(",RNP ", strRep).Replace(", RNP ", strRep).Replace(" RNP ", strRep);
    
			return strVal.Trim();
		}

		public static string ReplaceNameAbbrev(string strValue)
		{
			return ReplaceNameAbbrev(strValue, " ");
		}
		public static string ReplaceNameAbbrev(string strValue, string strReplace)
		{
			string strVal;
			string strRep;
    
			if (strReplace != " ") 
			{
				strRep = strReplace + " ";
			}
			else 
			{
				strRep = " ";
			}
			strVal = " " + strValue.Trim().ToUpper() + " ";
			//add space on ends to assist with replaces
    
			//comma ones must come first!

			strVal = strVal.Replace(",II ", strRep).Replace(", II ", strRep).Replace(" II ", strRep);
			strVal = strVal.Replace(",III ", strRep).Replace(", III ", strRep).Replace(" III ", strRep);

			strVal = strVal.Replace(",JR ", strRep).Replace(", JR ", strRep).Replace(" JR ", strRep);
			strVal = strVal.Replace(",J.R. ", strRep).Replace(", J.R. ", strRep).Replace(" J.R. ", strRep);
			strVal = strVal.Replace(",JR. ", strRep).Replace(", JR. ", strRep).Replace(" JR. ", strRep);
			strVal = strVal.Replace(",J.R ", strRep).Replace(", J.R ", strRep).Replace(" J.R ", strRep);
			strVal = strVal.Replace(",J R. ", strRep).Replace(", J R. ", strRep).Replace(" J R. ", strRep);
			strVal = strVal.Replace(",J. R. ", strRep).Replace(", J. R. ", strRep).Replace(" J. R. ", strRep);
			strVal = strVal.Replace(",J R ", strRep).Replace(", J R ", strRep).Replace(" J R ", strRep);

			strVal = strVal.Replace(",SR ", strRep).Replace(", SR ", strRep).Replace(" SR ", strRep);
			strVal = strVal.Replace(",S.R. ", strRep).Replace(", S.R. ", strRep).Replace(" S.R. ", strRep);
			strVal = strVal.Replace(",SR. ", strRep).Replace(", SR. ", strRep).Replace(" SR. ", strRep);
			strVal = strVal.Replace(",S.R ", strRep).Replace(", S.R ", strRep).Replace(" S.R ", strRep);
			strVal = strVal.Replace(",S R. ", strRep).Replace(", S R. ", strRep).Replace(" S R. ", strRep);
			strVal = strVal.Replace(",S. R. ", strRep).Replace(", S. R. ", strRep).Replace(" S. R. ", strRep);
			strVal = strVal.Replace(",S R ", strRep).Replace(", S R ", strRep).Replace(" S R ", strRep);

			return strVal.Trim();
		}

		public static String StateAbbrev(String strState)
		{
			strState = strState.ToUpper();

			strState = strState.Replace("ALABAMA", "AL");
			strState = strState.Replace("ALASKA", "AK");
			strState = strState.Replace("ARIZONA", "AZ").Replace(" ARIZ ", " AZ ").Replace(" ARIZ.", " AZ");
			strState = strState.Replace("ARKANSAS", "AR").Replace(" ARK ", " AR ").Replace(" ARK.", " AR");
			strState = strState.Replace("CALIFORNIA", "CA").Replace(" CALIF ", " CA ").Replace(" CALIF.", " CA");
			strState = strState.Replace("COLORADO", "CO");
			strState = strState.Replace("CONNECTICUT", "CT").Replace(" CONN ", " CT ").Replace(" CONN.", " CT");
			strState = strState.Replace("DELEWARE", "DE");
			strState = strState.Replace("WASHINGTON, D.C.", "DC").Replace("WASHINGTON, DC.", "DC").Replace("WASHINGTON, D.C", "DC");
			strState = strState.Replace("WASHINGTON, DC", "DC").Replace("WASHINGTON D.C.", "DC").Replace("WASHINGTON D.C", "DC");
			strState = strState.Replace("WASHINGTON, DC.", "DC").Replace("WASHINGTON DC", "DC").Replace("WASHINGTON D C", "DC");
			strState = strState.Replace("DISTRICT OF COLUMBIA", "DC").Replace("DIST OF COLUMBIA", "DC").Replace("DIST. OF COLUMBIA", "DC");
			strState = strState.Replace("FLORIDA", "FL").Replace(" FLOR ", " FL ").Replace(" FLOR.", " FL");
			strState = strState.Replace("GEORGIA", "GA");
			strState = strState.Replace("HAWAII", "HI");
			strState = strState.Replace("IDAHO", "ID");
			strState = strState.Replace("ILLINOIS", "IL");
			strState = strState.Replace("INDIANA", "IN");
			strState = strState.Replace("IOWA", "IOWA");
			strState = strState.Replace("KANSAS", "KS");
			strState = strState.Replace("KENTUCKY", "KY").Replace(" KENT.", " KY");
			strState = strState.Replace("LOUISIANA", "LA");
			strState = strState.Replace("MAINE", "ME");
			strState = strState.Replace("MARYLAND", "MD");
			strState = strState.Replace("MASSACHUSETTS", "MA").Replace(" MASS ", " MA ").Replace(" MASS.", " MA");
			strState = strState.Replace("MICHIGAN", "MI").Replace(" MICH ", " MI ").Replace(" MICH.", " MI");
			strState = strState.Replace("MINNESOTA", "MN").Replace(" MINN ", " MN ").Replace(" MINN.", " MN");
			strState = strState.Replace("MISSISSIPPI", "MI");
			strState = strState.Replace("MISSOURI", "MS");
			strState = strState.Replace("MONTANA", "MT");
			strState = strState.Replace("NEBRASKA", "NE").Replace(" NEB.", " NB");
			strState = strState.Replace("NEVADA", "NV").Replace(" NEV.", " NV");
			strState = strState.Replace("NEW HAMPSHIRE", "NH").Replace(" NEW HAMP ", " NH ").Replace(" NEW HAMP.", " NH");
			strState = strState.Replace("N HAMPSHIRE", "NH").Replace(" N. HAMPSHIRE ", " NH ");
			strState = strState.Replace("NEW JERSEY", "NJ");
			strState = strState.Replace("NEW MEXICO", "NM");
			strState = strState.Replace("NEW YORK", "NY");
			strState = strState.Replace("NORTH CAROLINA", "NC").Replace(" N CAROLINA", " NC").Replace(" N. CAROLINA", " NC");
			strState = strState.Replace("NORTH DAKOTA", "ND").Replace(" N DAKOTA", " ND").Replace(" N. DAKOTA", " ND");
			strState = strState.Replace("OHIO", "OH");
			strState = strState.Replace("OKLAHOMA", "OK");
			strState = strState.Replace("OREGON", "OR");
			strState = strState.Replace("PENNSYLVANIA", "PA").Replace(" PENN ", " PA ").Replace(" PENN.", " PA");
			strState = strState.Replace("RHODE ISLAND", "RI").Replace(" R ISLAND", " RI").Replace(" R. ISLAND", " RI");
			strState = strState.Replace("SOUTH CAROLINA", "SC").Replace(" S CAROLINA", " SC").Replace(" S. CAROLINA", " SC");
			strState = strState.Replace("SOUTH DAKOTA", "SD").Replace(" S DAKOTA", " SD").Replace(" S. DAKOTA", " SD");
			strState = strState.Replace("TENNESSEE", "TN").Replace(" TENN ", " TN ").Replace(" TENN.", " TN");
			strState = strState.Replace("TEXAS", "TX");
			strState = strState.Replace("UTAH", "UT");
			strState = strState.Replace("VERMONT", "VT");
			strState = strState.Replace("VIRGINIA", "VA");
			strState = strState.Replace("WASHINGTON", "WA").Replace(" WASH ", " WA ").Replace(" WASH.", " WA");
			strState = strState.Replace("WEST VIRGINIA", "WV").Replace(" W VIRGINIA", " WV").Replace(" W. VIRGINIA", " WV");
			strState = strState.Replace("WISCONSIN", "WI");
			strState = strState.Replace("WYOMING", "");

			strState = strState.Replace("AMERICAN SOMOA", "AS").Replace(" AM. SOMOA", " AS").Replace(" AM SOMOA", " AS");
			strState = strState.Replace("FEDERATED STATES OF MICRONESIA", "FM").Replace("FED. STATES OF MICRONESIA", "FM");
			strState = strState.Replace("FED STATES OF MICRONESIA", "FM").Replace("FED. ST. OF MICRONESIA", "FM");
			strState = strState.Replace("FED ST. OF MICRONESIA", "FM").Replace("FED. ST OF MICRONESIA", "FM");
			strState = strState.Replace("FED ST OF MICRONESIA", "FM").Replace("MICRONESIA", "FM");
			strState = strState.Replace("GUAM", "GU");
			strState = strState.Replace("MARSHALL ISLANDS", "MH").Replace("M. ISLANDS", "MH").Replace("MARSH. ISLANDS", "MH");
			strState = strState.Replace("NORTHERN MARIANA ISLANDS", "MP").Replace(" N MARIANA ISLANDS ", " MP ").Replace(" N. MARIANA ISLANDS", " MP");
			strState = strState.Replace("PALAU", "PW");
			strState = strState.Replace("PUERTO RICO", "PR");
			strState = strState.Replace("VIRGIN ISLANDS", "VI");

			return strState;
		}

		/// <summary>
		/// Reformat any string.  strSource, strFormat must be the same length and strFormat must contain
		/// UNIQUE characters.  example: strformat:12345678 strsource:19001122 stroutformat:56/78/1234
		/// Output: 11/22/1900
		/// </summary>
		/// <param name="strSource"></param>
		/// <param name="strFormat"></param>
		/// <param name="strOutFormat"></param>
		/// <returns></returns>
		public static String StringFormat(String strSource, String strFormat, String strOutFormat)
		{
			String strRet = "";
			int intTemp;
			if(strFormat.Length != strSource.Length)
			{
				throw new Exception("String formatting parameters incorrect");
			}

			for(int i=0; i<strOutFormat.Length; i++)
			{
				intTemp = strFormat.IndexOf(strOutFormat[i]);
				if(intTemp < 0)
				{
					strRet += strOutFormat.Substring(i, 1);
				}
				else
				{
					strRet += strSource.Substring(intTemp, 1);
				}
			}
			return strRet;
		}

        public static int ToNumberDefault(string strNumCheck, int intDefault)
        {
            int intRet = intDefault;
            if (IsNumber(strNumCheck))
            {
                intRet = Convert.ToInt32(strNumCheck);
            }
            return intRet;
        }

		public static String TrimLeft(String strValue, String strChar)
		{
			//trim beginning characters
			if(strValue.Length != 0)
			{
				while(strValue.Substring(0, 1) == strChar)
				{
					strValue = strValue.Substring(1);
					if(strValue.Length == 0)
					{
						break;
					}
				}
			}
			return strValue;
		}

		/// <summary>
		/// Encrypt text using default encryption key
		/// </summary>
 		public static string EncryptText(string strText)
		{
			//return Encrypt(strText, "&%#@?,:*");
			return Encrypt(strText, ENCRYPT_KEY);
		}

		/// <summary>
		/// Decrypt text using default encryption key
		/// </summary>
		public static string DecryptText(string strText)
		{
			//return Decrypt(strText, "&%#@?,:*");
			return Decrypt(strText, ENCRYPT_KEY);
		}

		/// <summary>
		/// The function used to encrypt the text
		/// </summary>
		private static string Encrypt(string strText, string strEncrKey)
		{
			byte[] byKey = {};
			byte[] IV = {18, 52, 86, 120, 144, 171, 205, 239};

			try 
			{
				//byKey = System.Text.Encoding.UTF8.GetBytes(Strings.Left(strEncrKey, 8));
				byKey = System.Text.Encoding.UTF8.GetBytes(strEncrKey.Substring(0, 8));

				DESCryptoServiceProvider des = new DESCryptoServiceProvider();
				byte[] inputByteArray = Encoding.UTF8.GetBytes(strText);
				MemoryStream ms = new MemoryStream();
				CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write);
				cs.Write(inputByteArray, 0, inputByteArray.Length);
				cs.FlushFinalBlock();
				return Convert.ToBase64String(ms.ToArray());
			}

			catch (Exception ex) 
			{
				return ex.Message;
			}

		}

		/// <summary>
		/// The function used to decrypt the text
		/// </summary>
		private static string Decrypt(string strText, string sDecrKey)
		{
			byte[] byKey = {};
			byte[] IV = {18, 52, 86, 120, 144, 171, 205, 239};
			byte[] inputByteArray = new byte[strText.Length + 1];

			try 
			{
				//byKey = System.Text.Encoding.UTF8.GetBytes(Strings.Left(sDecrKey, 8));
				byKey = System.Text.Encoding.UTF8.GetBytes(sDecrKey.Substring(0, 8));
				DESCryptoServiceProvider des = new DESCryptoServiceProvider();
				inputByteArray = Convert.FromBase64String(strText);
				MemoryStream ms = new MemoryStream();
				CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write);

				cs.Write(inputByteArray, 0, inputByteArray.Length);
				cs.FlushFinalBlock();
				System.Text.Encoding encoding = System.Text.Encoding.UTF8;

				return encoding.GetString(ms.ToArray());
			}

			catch (Exception ex) 
			{
				return ex.Message;
			}
		}

		/// <summary>
		/// Returns the ASCII character value of the given single-character string
		/// </summary>
		public static int Ascii(string strChr)
		{
			return (int)Encoding.ASCII.GetBytes(strChr)[0];
			// or return (int)Encoding.GetEncoding(System.Globalization.CultureInfo.CurrentCulture.TextInfo.ANSICodePage).GetBytes(S)[0];
			// casting or using Convert.ToInt32(strChr) only retrieves 1-127?
		}

		/// <summary>
		/// Returns the ASCII character value of the given character 
		/// </summary>
		public static int Ascii(char ch)
		{
			return (int)Encoding.ASCII.GetBytes(new char[] {ch}, 0, 1)[0];
		}

		public static string MakeDecimal(string strCash)
		{
			return MakeDecimal(strCash, false, false);
		}
		public static string MakeDecimal(string strCash, bool boolAllowNegative)
		{
			return MakeDecimal(strCash, boolAllowNegative, false);
		}
		public static string MakeDecimal(string strCash, bool boolAllowNegative, bool bool835)
		{
			//takes in cash value, adds decimals
			//will pad, add zeroes in front if needed
			//4 -> 0.04
			//123 -> 1.23
			//ignores existing punctuation
			string strValue;
			bool boolNeg;
    
			strValue = strCash;
    
			boolNeg = false;
			if (boolAllowNegative == true) 
			{
				if (UtilString.LikeStr(strValue, "*-") || UtilString.LikeStr(strValue, "-*"))
					boolNeg = true;
			}
    
			strValue = UtilString.StripNonInt(strValue);
			if (strValue == "") 
			{
				strValue = "0";
			}
    
			if (bool835) 
			{
				while (UtilString.LikeStr(strValue, "0?*"))
				{
					System.Windows.Forms.Application.DoEvents();
					strValue = UtilString.Mid(strValue, 2);
				}
				if (strValue.Length == 1) 
				{
					strValue = "0" + strValue;
				}
			}
			else 
			{
				while (strValue.Length < 3) 
				{
					System.Windows.Forms.Application.DoEvents();
					strValue = "0" + strValue;
				}
			}
    
			strValue = UtilString.Mid(strValue, 1, strValue.Length - 2) + "." + UtilString.Mid(strValue, strValue.Length - 1);
			if (boolNeg) 
			{
				strValue = "-" + strValue;
			}
			return strValue;
		}

		public static string CashDecimal(string strCash)
		{
			return CashDecimal(strCash, false, false, "");
		}
		public static string CashDecimal(string strCash, bool boolAllowNegative)
		{
			return CashDecimal(strCash, boolAllowNegative, false, "");
		}
		public static string CashDecimal(string strCash, bool boolAllowNegative, bool bool835)
		{
			return CashDecimal(strCash, boolAllowNegative, bool835, "");
		}
		public static string CashDecimal(string strCash, bool boolAllowNegative, bool bool835, string CashMustHaveDECIMAL)
		{
			//takes in a number with a single decimal and converts it to a cash amount without decimals
			//no decimal is taken to mean that 2 trailing zeroes should be added
			//also removes leading zeroes
			//examples:
			//  04.65 = 465
			//  04.6  = 460
			//  04    = 400
			//returns zero if no value found
			string strChar;
			string strDollars;
			string strCents;
			int intX;
			bool boolDecimal;
			bool boolNeg;
			string strOut;
    
			if (strCash == "") 
			{
				return "0"; 
			}
    
			boolNeg = false;
			if (boolAllowNegative == true) 
			{
				if (UtilString.LikeStr(strCash, "*-") || UtilString.LikeStr(strCash, "-*"))
					boolNeg = true;
			}
    
			boolDecimal = false;
			strDollars = "";
			strCents = "";
    
			for (intX = 1; intX <= strCash.Length; intX++) 
			{
				System.Windows.Forms.Application.DoEvents();
        
				strChar = UtilString.Mid(strCash, intX, 1);
				if (strChar == ".") 
				{
					boolDecimal = true;
				}
				else if (IsNumber(strChar)) 
				{
					if (boolDecimal) 
					{
						//means decimal found, put value in cents
						strCents = strCents + strChar;
					}
					else 
					{
						strDollars = strDollars + strChar;
					}
				}
			}
    
			if (strCents.Length > 2) 
			{
				strCents = UtilString.Mid(strCents, 1, 2);
			}
			while (strCents.Length < 2) 
			{
				strCents = strCents + "0";
			}
    
			while (UtilString.LikeStr(strDollars, "0*"))
			{
				System.Windows.Forms.Application.DoEvents();
        
				strDollars = UtilString.Mid(strDollars, 2);
			}
    
			if ((strDollars == "") && (strCents == "")) 
			{
				strCents = "0";
				//return zero if no value found
			}
    
			if (boolNeg == true) 
			{
				strDollars = "-" + strDollars;
			}
    
			strOut = strDollars + strCents;
			if ((strOut == "0") || (strOut == "00")) 
			{
				strOut = "000";
			}
			if ((bool835 == true) && (strOut.Length == 3) && (UtilString.Mid(strOut, 1, 1) == "0")) 
			{
				strOut = UtilString.Mid(strOut, 2);
			}
			return strDollars + strCents;
		}

		public static string StripNonInt(string strNumber)
		{
			int intX;
			string strTemp;
			string strChar;
    
			strTemp = "";
    
			for (intX = 1; intX <= strNumber.Length; intX++) 
			{
				strChar = UtilString.Mid(strNumber, intX, 1);
				if (IsNumber(strChar)) 
				{
					strTemp = strTemp + strChar;
				}
			}
    
			if (strTemp == "") 
			{
				strTemp = "0";
			}
			return strTemp;
		}

	}
}
