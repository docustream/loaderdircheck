﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Windows.Forms;
using System.Linq;
using System.Text;

namespace dnUtil
{
    #region RowData Structure
    public struct RowData
    {
        //public int ClaimIndex;
        //public string strPayer;
        //public string strPayerID;
        //public string strClaimType;
        //public string strImagePath;
        //public string strNewImagePath;
        public string strFolderPath;
    }
    #endregion
    /// <summary>
    /// Functions for a DataGridView with the following Collumn Names:
    /// ImageNum, ImageDate, ImagePrintJob, ImagePayer, ImagePayerID, ImageClaimType, ClaimIndex, ImagePath, ImageNewPath
    /// </summary>
    static class UtilDGV
    {
        #region SetMax
        public static int formSetDGVMaxIndex(DataGridView DGV)
        {
            ArrayList arrTempSelected = new ArrayList(formFindArrSelectedfromDG(DGV));
            arrTempSelected.Sort();
            //intMaxIndex = Convert.ToInt32(arrTempSelected[0]);
            return Convert.ToInt32(arrTempSelected[0]);
        }
        #endregion
        #region SetMin
        public static int formSetDGVMinIndex(DataGridView DGV)
        {
            ArrayList arrTempSelected = new ArrayList(formFindArrSelectedfromDG(DGV));
            arrTempSelected.Sort();
            //intMinIndex = Convert.ToInt32(arrTempSelected[0]);
            return Convert.ToInt32(arrTempSelected[arrTempSelected.Count - 1]);
        }
        #endregion
        #region FindCellValue
        /// <summary>
        /// Finds the row with the Given index value and returns the desired cell value
        /// </summary>
        /// <param name="DGV">DataGridView</param>
        /// <param name="index">index of desired row</param>
        /// <param name="strCellName">Name of cell desired</param>
        /// <returns>Value in Cell, or "" if not found</returns>
        public static string formFindValueofCell(DataGridView DGV, int index, string strCellName)
        {
            foreach (DataGridViewRow Row in DGV.Rows)
            {
                if (Row.Cells["ClaimIndex"].Value.ToString() == index.ToString())
                    return Row.Cells[strCellName].Value.ToString();
            }
            return "";
        }
        /// <summary>
        /// OVERLOAD: Finds the row with the Given index value and returns the desired cell value
        /// </summary>
        /// <param name="DGV">DataGridView</param>
        /// <param name="indexSearch">Index to search for to identify row</param>
        /// <param name="strCellNameWanted">Name of Cell wanted</param>
        /// <param name="strCellSearch">Name of Cell to search for index (Defaults to = "ClaimIndex")</param>
        /// <returns>Value in cell</returns>
        public static string formFindValueofCell(DataGridView DGV, string indexSearch, string strCellNameWanted, string strCellSearch = "ClaimIndex")
        {
            foreach (DataGridViewRow Row in DGV.Rows)
            {
                if (Row.Cells[strCellSearch].Value.ToString() == indexSearch.ToString())
                    return Row.Cells[strCellNameWanted].Value.ToString();
            }
            return "";
        }
        /// <summary>
        /// OVERLOAD: Finds the row with the Given index value and returns the desired cell value
        /// </summary>
        /// <param name="Row">if know DataGridViewRow use, instead of whole DGV</param>
        /// <param name="index">index of desired row</param>
        /// <param name="strCellName">Name of cell desired</param>
        /// <returns>Value in Cell, or "" if not found</returns>
        public static string formFindValueofCell(DataGridViewRow Row, int index, string strCellName)
        {
            if (Row.Cells["ClaimIndex"].Value.ToString() == index.ToString())
                return Row.Cells[strCellName].Value.ToString();
            else
                return "";
        }
        #endregion
        #region find ClaimIndex for Given Row
        /// <summary>
        /// Returns "ClaimIndex" Collumn Value for given row, (the two can differ)
        /// </summary>
        /// <param name="DGV">DataGridView</param>
        /// <param name="countIndex">Row Count Desired</param>
        /// <returns>ClaimIndex</returns>
        public static string formFindRowClaimIndex(DataGridView DGV, int countIndex)
        {
            return DGV.Rows[countIndex].Cells["ClaimIndex"].Value.ToString();
        }
        #endregion
        #region SelectRow
        /// <summary>
        /// Select Cell based on Index
        /// </summary>
        /// <param name="DGV">DataGridView Containing Claims</param>
        /// <param name="Index">Index number to select</param>
        public static void formSelectNewDataGridRow(DataGridView DGV, int Index)
        {
            for (int i = 0; i < DGV.Rows.Count; i++)
            {
                if (DGV.Rows[i].Cells["ClaimIndex"].Value.ToString() == Index.ToString())
                {
                    DGV.Rows[i].Selected = true;
                }
            }
        }
        /// <summary>
        /// Reselects the First/Selected Claim
        /// Useful after ImageList Load/reload
        /// </summary>
        /// <param name="DGV">DGV to select claim out of</param>
        /// <param name="boolKeepSelected">if false (default) Will select first Images, otherwise will select currently selected image</param>
        public static void formSelectFirstClaim(DataGridView DGV, bool boolKeepSelected = false)
        {
            if (DGV.RowCount > 0)
            {
                int tempSelectedClaimIndex = 1;
                if (boolKeepSelected)
                    tempSelectedClaimIndex = formFindSelectedIndex(DGV);
                //****Trick to deselect rows, allowing for new selection****\\
                DGV.MultiSelect = false;
                DGV.MultiSelect = true;
                formSelectNewDataGridRow(DGV, tempSelectedClaimIndex);
            }
        }
        #endregion
        #region Insert Data into Cell
        /// <summary>
        /// Inserts Data into the Desired Cell, using the Claim Index to find the row
        /// </summary>
        /// <param name="DGV">DataGridView</param>
        /// <param name="strValue">Value to insert</param>
        /// <param name="strCellName">Cell to insert into</param>
        /// <param name="indexValue">Claim Index</param>
        /// <param name="indexCellName">Name of Cell to search for Index Default: ClaimIndex</param>
        public static void formInsertDatatoCell(DataGridView DGV, string strValue, string strCellName, string indexValue, string indexCellName = "ClaimIndex")
        {
            for (int i = 0; i < DGV.Rows.Count; i++)
            {
                if (DGV.Rows[i].Cells[indexCellName].Value.ToString() == indexValue.ToString())
                {
                    DGV.Rows[i].Cells[strCellName].Value = Convert.ChangeType(strValue,DGV.Rows[i].Cells[strCellName].ValueType) ;
                }
            }
        }
        #endregion
        #region ClaimType
        /// <summary>
        /// Inserts Selected ClaimType into DataGridView
        /// </summary>
        /// <param name="DGV">DataGridView with selected Claims</param>
        /// <param name="strClaimType">Claim Types</param>
        public static void formInsertCalimTypeDataGrid(DataGridView DGV, string strClaimType)
        {
            for (int i = 0; i < DGV.Rows.Count; i++)
            {
                if (DGV.Rows[i].Selected == true)
                {
                    DGV.Rows[i].Cells["ImageClaimType"].Value = strClaimType;
                }
            }
            Application.DoEvents();
        }
        #endregion
        #region Payer
        /// <summary>
        /// Enters the Payer Name and PayerID into the Datagrid for the selected claims
        /// </summary>
        /// <param name="DGV">DataGridView with selected claims</param>
        /// <param name="strPayerName">Payer Name</param>
        /// <param name="strPayerID">PayerID</param>
        public static void formInsertPayerInfoDataGrid(DataGridView DGV, string strPayerName, string strPayerID)
        {
            for (int i = 0; i < DGV.Rows.Count; i++)
            {
                if (DGV.Rows[i].Selected == true)
                {
                    DGV.Rows[i].Cells["ImagePayerID"].Value = strPayerID;
                    DGV.Rows[i].Cells["ImagePayer"].Value = strPayerName;
                }
            }
            Application.DoEvents();
        }
        #endregion
        #region MulitSelected
        /// <summary>
        /// Find an array of selected Claims from the DataGridView
        /// </summary>
        /// <param name="DGV">DataGridView selected</param>
        /// <param name="strGGName">Name of the Collumn to return</param>
        /// <returns>ArrayList of selected Claims</returns>
        public static ArrayList formFindArrSelectedfromDG(DataGridView DGV, string strCollumn = "ClaimIndex")
        {
            ArrayList arrSelectedClaims = new ArrayList();
            for (int i = 0; i <= DGV.SelectedRows.Count - 1; i++)
            {
                //****Find list then add Batch name to Arraylist****\\
                arrSelectedClaims.Add(DGV.SelectedRows[i].Cells[strCollumn].Value.ToString());
            }
            return arrSelectedClaims;
        }
        /// <summary>
        /// Find an array of NON-selected Batchnames from the DataGridView
        /// </summary>
        /// <param name="DGV">DataGridView</param>
        /// <param name="strCollumn">Name of Collumn to return</param>
        /// <returns>ArrayList of non-selected Claims</returns>
        public static ArrayList formFindArrNOTSelectedfromDG(DataGridView DGV, string strCollumn = "ClaimIndex")
        {
            ArrayList arrNOTSelectedClaims = new ArrayList();
            for (int i = 0; i <= DGV.Rows.Count - 1; i++)
            {
                if (DGV.Rows[i].Selected ==false)
                    //****Find list then add Batch name to Arraylist****\\
                    arrNOTSelectedClaims.Add(DGV.Rows[i].Cells[strCollumn].Value.ToString());
            }
            return arrNOTSelectedClaims;
        }
        #endregion
        #region FirstSelected ImagePath
        /// <summary>
        /// Find the Selected Claims path, find first page and return Path
        /// </summary>
        /// <param name="structGG">DataGrid Group Structure currently selected</param>
        /// <returns>Path of Coverpage for first selected Claim</returns>
        public static string formFindSelectedImageName(DataGridView DGV)
        {
            try
            {
                string strImageName = "";
                //****If at least one Batch is selected****\\
                if (DGV.SelectedRows.Count > 0)
                {
                    //****Find the Workflow for selected batch from hidden Workflow Column in DataGrid****\\
                    strImageName = DGV.SelectedRows[0].Cells["clmFolder"].Value.ToString();
                    return strImageName;
                }
            }
            catch
            { }
            return "";
        }
        #endregion
        #region FirstSelected Index
        /// <summary>
        /// Find the Selected Claims Index
        /// </summary>
        /// <param name="structGG">DataGrid Group Structure currently selected</param>
        /// <returns>Index for first selected Claim</returns>
        public static int formFindSelectedIndex(DataGridView DGV)
        {
            try
            {
                string strImageName = "";
                //****If at least one Batch is selected****\\
                if (DGV.SelectedRows.Count > 0)
                {
                    //****Find the Workflow for selected batch from hidden Workflow Column in DataGrid****\\
                    strImageName = DGV.SelectedRows[0].Cells["ClaimIndex"].Value.ToString();
                    return Convert.ToInt32(strImageName);
                }
            }
            catch
            { }
            return 0;
        }
        #endregion
        #region CheckDGV for Image
        /// <summary>
        /// Checks to see if Image already exists in the DGV
        /// </summary>
        /// <param name="DGV">DGV with list of Images</param>
        /// <param name="ImagePath">New Image Path to check</param>
        /// <returns>Boolean true (Exists) or False (Does not Exist)</returns>
        public static bool formCheckImageAlreadyLoaded(DataGridView DGV, string ImagePath, string CollomnName = "ImagePath")
        {
            bool boolExists = false;
            foreach (DataGridViewRow Row in DGV.Rows)
            {
                string temp = Row.Cells[CollomnName].Value.ToString();
                if (Row.Cells[CollomnName].Value.ToString() == ImagePath)
                    boolExists = true;
            }
            return boolExists;
        }
        #endregion
        #region DeleteFromDGV
        /// <summary>
        /// Deletes every selected row and returns the array of Deleted Images' paths
        /// </summary>
        /// <param name="DGV">DataGridView to delete from</param>
        /// <returns>List of Deleted Images' Paths</returns>
        public static ArrayList formDeleteRowFromDGV(DataGridView DGV)
        {
            ArrayList DeletedRows = new ArrayList(formFindArrSelectedfromDG(DGV, "ImagePath"));
            foreach (DataGridViewRow rowDelete in DGV.SelectedRows)
            {
                DGV.Rows.Remove(rowDelete);
            }
            return DeletedRows;
        }
        #endregion
        #region Rows to Upload
        /// <summary>
        /// Find rows with all Data Completed and Select them
        /// Claims need ImagePath, PayerID, Payer, ClaimType all filled in
        /// </summary>
        /// <param name="DGV">DataGridView with list of claims</param>
        /// <param name="strArchiveDir">Destenation to set new path to</param>
        //public static void formSelectRowstoUpload(DataGridView DGV, string strArchiveDir)
        //{
        //    for (int i = 0; i < DGV.Rows.Count; i++)
        //    {
        //        DGV.Rows[i].Selected = false;
        //        RowData RD = new RowData();
        //        //****Place in struct****\\
        //        RD = formFindRDinfo(DGV.Rows[i],formFindRowClaimIndex(DGV, i));
        //        //****Check if claim completed****\\
        //        if (RD.strNewImagePath != "")
        //            DGV.Rows[i].Selected = true;
        //        if (formCheckRowStatus(RD))
        //        {
        //            formAddNewImagePath(DGV, RD, strArchiveDir, i);
        //        }
        //    }
        //}
        /// <summary>
        /// Return the number of selected claims
        /// </summary>
        /// <param name="DGV">Data Grid View of Claims</param>
        /// <param name="boolSelected">(Default)True: returns selected count, False: returns non Selected</param>
        /// <returns>Number of claims</returns>
        public static int CountSelectedClaimsinDGV(DataGridView DGV, bool boolSelected = true)
        {
            int intCount = 0;
            foreach (DataGridViewRow Row in DGV.Rows)
            {
                if (Row.Selected == boolSelected)
                    intCount++;
            }
            return intCount;
        }
        #endregion
        #region Add ImageNewPath to DGV
        /// <summary>
        /// Select Claim and add new ImagePath
        /// (ArchivePath + FileName [Sub "XXX" for PayerID]
        /// </summary>
        /// <param name="DGV">DataGridView of claims</param>
        /// <param name="RD">Selected Row Data</param>
        /// <param name="strArchiveDir">Archive Dir for new Path</param>
        /// <param name="CurrRowIndex">Current row count</param>
        //public static void formAddNewImagePath(DataGridView DGV, RowData RD, string strArchiveDir, int CurrRowIndex)
        //{
        //    RD.strNewImagePath = dnUtil.UtilFile.FileName(RD.strImagePath);
        //    RD.strNewImagePath = strArchiveDir + RD.strNewImagePath;
        //    RD.strNewImagePath = replaceTifDateinPath(RD.strNewImagePath);
        //    RD.strNewImagePath = RD.strNewImagePath.Replace("XXXX", RD.strPayerID);
        //    string strExt = System.IO.Path.GetExtension(RD.strNewImagePath);
        //    RD.strNewImagePath = dnUtil.UtilFile.findNameMinusNum(RD.strNewImagePath) + RD.ClaimIndex.ToString("d4");
        //    RD.strNewImagePath = RD.strNewImagePath + strExt;
        //    bool boolFileExists = System.IO.File.Exists(RD.strNewImagePath);
        //    bool boolFileinDGV = formCheckImageAlreadyLoaded(DGV, RD.strNewImagePath, "ImageNewPath");
        //    while (boolFileExists || boolFileinDGV)
        //    {
        //        RD.strNewImagePath = dnUtil.UtilFile.newNamePlusNum(RD.strNewImagePath);
        //        boolFileExists = System.IO.File.Exists(RD.strNewImagePath);
        //        boolFileinDGV = formCheckImageAlreadyLoaded(DGV, RD.strNewImagePath, "ImageNewPath");
        //    }
        //    formInsertDatatoCell(DGV, RD.strNewImagePath, "ImageNewPath", (RD.ClaimIndex).ToString());
        //    DGV.Rows[CurrRowIndex].Selected = true;
        //}
        /// <summary>
        /// Replaces the Printed date in the file name with Today's Date
        /// </summary>
        /// <param name="strOrigTifName">Original File Name</param>
        /// <returns>File Name with Todays date in place of the Printed Date</returns>
        //public static string replaceTifDateinPath(string strOrigTifName)
        //{
        //    string strPath = strOrigTifName.Substring(0, strOrigTifName.LastIndexOf("\\") + 1);
        //    string strTifname = System.IO.Path.GetFileName(strOrigTifName);
        //    string strOrigDate = strTifname.Substring(12, 8);
        //    //string strTodayDate = DocuHLTransmit.DnUtil.ChooserTransmitFunctions.TodayDate();
        //    strTifname = strTifname.Replace(strOrigDate, strTodayDate);
        //    return strPath + strTifname;
        //}
        #endregion
        #region RowData Struct Functions
        /// <summary>
        /// Fill a RowData Struct from given row
        /// </summary>
        /// <param name="Row">Selected row from DataGridView</param>
        /// <param name="ClaimIndex">ClaimIndex of selected row</param>
        /// <returns>Returns RowData Stuct</returns>
        public static DataGridViewRow formFindRow( string strValue)
        {
            DataGridViewRow Row = new DataGridViewRow();
            Row.SetValues(strValue);
            //RowData RD = new RowData();
            
            //RD.strFolderPath = strValue;
            //RD.ClaimIndex = Convert.ToInt32(ClaimIndex);
            //RD.strImagePath = formFindValueofCell(Row, RD.ClaimIndex, "ImagePath");
            //RD.strPayerID = formFindValueofCell(Row, RD.ClaimIndex, "ImagePayerID");
            //RD.strPayer = formFindValueofCell(Row, RD.ClaimIndex, "ImagePayer");
            //RD.strClaimType = formFindValueofCell(Row, RD.ClaimIndex, "ImageClaimType");
            //RD.strNewImagePath = formFindValueofCell(Row, RD.ClaimIndex, "ImageNewPath");
            return Row;
        }
        /// <summary>
        /// Checks to see if ImagePath, PayerID, Payer, Claim Type is blank or NewImagePath is not blank
        /// </summary>
        /// <param name="RD">RowData</param>
        /// <returns>True: All data filled in, False: missing Data</returns>
        //private static bool formCheckRowStatus(RowData RD)
        //{
        //    bool boolStatus = true;
        //    if (RD.strImagePath == "")
        //        boolStatus = false;
        //    if (RD.strPayerID == "")
        //        boolStatus = false;
        //    if (RD.strPayer == "")
        //        boolStatus = false;
        //    if (RD.strClaimType == "")
        //        boolStatus = false;
        //    if (RD.strNewImagePath != "")
        //        boolStatus = false;
        //    return boolStatus;
        //}
        #endregion
        #region Index ReNumber
        /// <summary>
        /// Renumber the Claim index to sequencal numbers
        /// </summary>
        /// <param name="DGV">DataGridView</param>
        public static void RenumberIndex(DataGridView DGV)
        {
            for (int i = 0; i < DGV.Rows.Count; i++)
            {
                string temp = DGV.Rows[i].Cells["ClaimIndex"].Value.ToString();
                formInsertDatatoCell(DGV, (i+1).ToString(), "ClaimIndex", DGV.Rows[i].Cells["ImagePath"].Value.ToString(), "ImagePath");
            }
        }
        #endregion
    }
}
