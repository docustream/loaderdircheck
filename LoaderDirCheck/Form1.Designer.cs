﻿namespace LoaderDirCheck
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.txtLoaderXPath = new System.Windows.Forms.TextBox();
            this.txtLogFilePath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnViewLog = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.PB = new System.Windows.Forms.ProgressBar();
            this.lblStatus = new System.Windows.Forms.Label();
            this.DGVFolders = new System.Windows.Forms.DataGridView();
            this.clmFolder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuDGV = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuItemCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.DGVFolders)).BeginInit();
            this.contextMenuDGV.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtLoaderXPath
            // 
            this.txtLoaderXPath.Location = new System.Drawing.Point(202, 35);
            this.txtLoaderXPath.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.txtLoaderXPath.Name = "txtLoaderXPath";
            this.txtLoaderXPath.Size = new System.Drawing.Size(630, 31);
            this.txtLoaderXPath.TabIndex = 0;
            // 
            // txtLogFilePath
            // 
            this.txtLogFilePath.Location = new System.Drawing.Point(202, 100);
            this.txtLogFilePath.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.txtLogFilePath.Name = "txtLogFilePath";
            this.txtLogFilePath.Size = new System.Drawing.Size(630, 31);
            this.txtLogFilePath.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 35);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "LoaderDB Path:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 106);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 25);
            this.label2.TabIndex = 3;
            this.label2.Text = "Log File Path:";
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(848, 81);
            this.btnStop.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(150, 44);
            this.btnStop.TabIndex = 4;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnViewLog
            // 
            this.btnViewLog.Location = new System.Drawing.Point(848, 137);
            this.btnViewLog.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.btnViewLog.Name = "btnViewLog";
            this.btnViewLog.Size = new System.Drawing.Size(150, 44);
            this.btnViewLog.TabIndex = 5;
            this.btnViewLog.Text = "View Log";
            this.btnViewLog.UseVisualStyleBackColor = true;
            this.btnViewLog.Click += new System.EventHandler(this.btnViewLog_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(848, 25);
            this.btnStart.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(150, 44);
            this.btnStart.TabIndex = 6;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(848, 192);
            this.btnExit.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(150, 44);
            this.btnExit.TabIndex = 7;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // PB
            // 
            this.PB.Location = new System.Drawing.Point(30, 192);
            this.PB.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.PB.Name = "PB";
            this.PB.Size = new System.Drawing.Size(806, 44);
            this.PB.TabIndex = 8;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(24, 162);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(98, 25);
            this.lblStatus.TabIndex = 9;
            this.lblStatus.Text = "Progress";
            // 
            // DGVFolders
            // 
            this.DGVFolders.AllowUserToAddRows = false;
            this.DGVFolders.AllowUserToDeleteRows = false;
            this.DGVFolders.AllowUserToOrderColumns = true;
            this.DGVFolders.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.DGVFolders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVFolders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmFolder});
            this.DGVFolders.ContextMenuStrip = this.contextMenuDGV;
            this.DGVFolders.Location = new System.Drawing.Point(30, 250);
            this.DGVFolders.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.DGVFolders.MultiSelect = false;
            this.DGVFolders.Name = "DGVFolders";
            this.DGVFolders.Size = new System.Drawing.Size(968, 310);
            this.DGVFolders.TabIndex = 10;
            // 
            // clmFolder
            // 
            this.clmFolder.HeaderText = "Folder Path";
            this.clmFolder.Name = "clmFolder";
            this.clmFolder.Width = 425;
            // 
            // contextMenuDGV
            // 
            this.contextMenuDGV.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.contextMenuDGV.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemCopy,
            this.MenuItemOpen});
            this.contextMenuDGV.Name = "contextMenuDGV";
            this.contextMenuDGV.Size = new System.Drawing.Size(150, 76);
            // 
            // MenuItemCopy
            // 
            this.MenuItemCopy.Name = "MenuItemCopy";
            this.MenuItemCopy.Size = new System.Drawing.Size(149, 36);
            this.MenuItemCopy.Text = "Copy";
            this.MenuItemCopy.Click += new System.EventHandler(this.MenuItemCopy_Click);
            // 
            // MenuItemOpen
            // 
            this.MenuItemOpen.Name = "MenuItemOpen";
            this.MenuItemOpen.Size = new System.Drawing.Size(149, 36);
            this.MenuItemOpen.Text = "Open";
            this.MenuItemOpen.Click += new System.EventHandler(this.MenuItemOpen_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1022, 583);
            this.Controls.Add(this.DGVFolders);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.PB);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.btnViewLog);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtLogFilePath);
            this.Controls.Add(this.txtLoaderXPath);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.Name = "Form1";
            this.Text = "Loader Directory Check (1.1.6)";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGVFolders)).EndInit();
            this.contextMenuDGV.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtLoaderXPath;
        private System.Windows.Forms.TextBox txtLogFilePath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnViewLog;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label lblStatus;
        public System.Windows.Forms.ProgressBar PB;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmFolder;
        public System.Windows.Forms.DataGridView DGVFolders;
        private System.Windows.Forms.ContextMenuStrip contextMenuDGV;
        private System.Windows.Forms.ToolStripMenuItem MenuItemCopy;
        private System.Windows.Forms.ToolStripMenuItem MenuItemOpen;
        private System.Windows.Forms.Timer timer1;
    }
}

